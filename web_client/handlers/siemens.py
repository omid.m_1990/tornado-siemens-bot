#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from tornado.web import RequestHandler, HTTPError

from web_client.functions.func import *


# from web_client.functions.functions import process_data, get_euro


class SiemensHandler(RequestHandler):
    def get(self):
        # self.write("Hello, world")
        self.render("index.html", name="Omid")


class APISiemensHandler(RequestHandler):
    pass
    # def get(self, _data):
    #     print(_data)
    #     # number = tracking_code()
    #     res = get_message(1,_data)
    #
    #     self.finish(
    #         dict(status=True, value=dict(name=res))
    #     )

    # def post(self, *args, **kwargs):
    #     _data = self.get_argument('data')
    #     res = get_message(1, _data)
    #     self.finish(
    #         dict(status=True, value=dict(name=res))
    #     )


class GetDataHandler(RequestHandler):
    def get(self):
        try:
            # euro = get_euro()
            euro = 0
            source = get_all_source()
            self.render("get-data.html", data=[], euro=euro, source_ls=source)
        except Exception as e:
            print(e)
            self.render("get-data.html", data=[], euro=0, source_ls=[])

    def post(self):
        try:
            default = dict()
            products_data = self.get_argument('products-data')
            default['euro'] = self.get_argument('euro')
            default['source'] = self.get_argument('source')
            default['my_ratio'] = self.get_argument('my-ratio')
            default['customer_ratio'] = self.get_argument('customer-ratio')
            default['statement'] = self.get_argument('statement')
            default['delivery'] = self.get_argument('delivery')
            default['remarks'] = self.get_argument('remarks')
            default['customer'] = self.get_argument('customer')

            js = process_data(products_data, default)
            # siemens_request(js, False)
            request_number = tracking_code()

            self.render("validation.html", data=js, default=default, request_number=request_number)
        except Exception as e:
            print(e)
            self.render("validation.html", data=[], default={}, request_number=0)

    def put(self):
        try:
            js = json.loads(self.get_argument("js", None))
            print("siemens request for update")
            siemens_request([js], True)
            self.write({"status": True, "msg": 'successfully', "value": js})
        except Exception as e:
            print(e)
            self.write({"status": False, "msg": 'خطا', "value": {}})


class DownloadHandler(RequestHandler):
    def get(self, file_name):

        _file_dir = os.path.join(Configs.app_root, "statics", "temp")
        # _file_path = "%s/%s" % (_file_dir, file_name)
        _file_path = os.path.join(_file_dir, file_name)
        if not file_name or not os.path.exists(_file_path):
            raise HTTPError(404)
        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
        with open(_file_path, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()
                        return
            except:
                raise HTTPError(404)
        raise HTTPError(500)


class ProductShowHandler(RequestHandler):
    def get(self, _request):
        try:
            # _request = args[0]
            if _request == 'all':
                pr_ls = SysProducts().get_all(20)
                # print(pr_ls)
            elif _request == 'lapmd':
                pr_ls = SysProducts().get_all_no_pmd(20)
            elif _request == 'validpmd':
                pr_ls = SysProducts().get_all_valid_pmd(20)

            else:
                pr_ls = []
            self.render("show_products.html", data=pr_ls, title='Siemens Bot', request_=_request)
        except Exception as e:
            print(e)
            self.render("show_products.html", data=[], title='Siemens Bot')

    def post(self, *args, **kwargs):
        try:
            act = self.get_argument("act", None)
            if act == 'lapmd':
                pr_ls = SysProducts().get_all_no_pmd(5000)
            elif act == 'validpmd':
                pr_ls = SysProducts().get_all_valid_pmd(5000)
            elif act == 'all':
                pr_ls = SysProducts().get_all(8000)
            else:
                raise Exception("Empty! ;) gErefti mA rO?")
            path = write_text(pr_ls, act)
            if path:
                path = os.path.join("static", "temp", '{}.txt'.format(act))
                self.write({"status": True, "msg": 'successfully', "value": path})
            else:
                self.write({"status": False, "msg": 'Error create file', "value": ''})

        except Exception as e:
            self.write({"status": False, "msg": e, "value": ''})


class ProductInsertHandler(RequestHandler):
    # def get(self):
    #     # res = get_message(1, _data)
    #     euro = get_euro()
    #     self.render("get-data.html", data = [], euro=euro)

    def post(self):
        try:
            data = self.get_argument("form_data", None)
            _act = self.get_argument("act", None)
            _request_number = self.get_argument("request_number", None)
            customer = self.get_argument("customer", None)
            data_js = json.loads(data)
            if _act == "excel":
                _remark = self.get_argument("remark", None)
                export_pmd_dt = self.get_argument("export_pmd_dt", None)
                sm = telegram_sum_price_message(data_js, _request_number)
                file_name = create_customer_excel(data_js, _request_number, _remark,customer,export_pmd_dt)
                if file_name:
                    path = os.path.join("statics", "temp", file_name)

                    self.write({"status": True, "msg": 'successfully', "value": {"path": path, "report": sm}})
                else:
                    self.write({"status": False, "msg": 'Error', "value": file_name})
            elif _act == "savedatabase":
                siemens_rqst = self.get_argument("requested", None)
                if not siemens_rqst:
                    siemens_request(data_js, True)

                status = save_log(data_js, _request_number,customer)
                if status:
                    self.write({"status": True, "msg": status, "value": status})
                else:
                    self.write({"status": False, "msg": status, "value": status})
        except Exception as e:
            self.write({"status": False, "msg": e, "value": ''})


class PriceDataHandler(RequestHandler):
    def post(self):
        _code = self.get_argument("code", None)
        source = self.get_argument("source", None)
        rawcode = _code.replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-', '').replace(' ', '')
        pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
        if pr:
            log=dict()
            source_ls = get_all_source(pr)
            res = SysLogs(p_id=pr['id'], source_id=source).get_10last_by_pid_source()

            log['max_source']= SysLogs(p_id=pr['id'], source_id=source).get_max_price_by_source()
            log['min_source']= SysLogs(p_id=pr['id'], source_id=source).get_min_price_by_source()
            log['min'] = SysLogs(p_id=pr['id']).get_min_price()
            log['max'] = SysLogs(p_id=pr['id']).get_max_price()
            log['last'] = SysLogs(p_id=pr['id']).get_last()
            # print(res)
            self.write({"status": True, "msg": 'successfully', "value": dict(logs=res, source=source_ls, report=log)})
        else:
            self.write({"status": False, "msg": 'Error', "value": ''})
        return


class ProductCheckHandler(RequestHandler):
    def post(self):
        _act = self.get_argument("act", None)
        _code = self.get_argument("code", None)
        rawcode = _code.replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-', '').replace(' ', '')
        if _act == "exist":
            pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
            if pr:
                # print(pr)
                dt_pmd_ratio = dt_pmd_ratio_search(pr['order_number'])
                if dt_pmd_ratio:
                    pr['dt_pmd_ratio'] = dt_pmd_ratio['value']
                    pr['dt_ratio_root'] = dt_pmd_ratio['root_name']
                else:
                    pr['dt_pmd_ratio'] = 0
                    pr['dt_ratio_root'] = ''
                self.write({"status": True, "msg": 'successfully', "value": pr})
            else:
                print('not in database code. do not send request')
                # js = vip_get_data([_code],{})
                # siemens_request(js, False)
                # if js['status']:
                #     self.write({"status": True, "msg": 'successfully', "value": js[0]})
                # else:
                self.write({"status": False, "msg": 'Error', "value": ''})
        elif _act == "like":
            print("Autocomplete")
            pr_ls = SysProducts(rawcode=rawcode).get_all_by_rawcode()
            if pr_ls:
                # print(pr_ls)
                self.write({"status": True, "msg": 'successfully', "value": pr_ls})
            else:
                self.write({"status": False, "msg": 'Error', "value": []})
        return


class PmdHandler(RequestHandler):
    def get(self):
        try:
            self.render("pmd_insert.html")
        except Exception as e:
            print(e)

    def post(self):
        try:
            _act = self.get_argument("act", None)
            if _act == 'pmd-ls-update':
                res = ''
                data = self.get_argument("pmddata", None)
                data_js = json.loads(data)
                _date = datetime.datetime.now()
                for i in data_js:
                    rawcode = i['code'].replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-', '')
                    pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
                    if pr:
                        res = SysProducts(id=pr['id']).update(pmd=i['pmd'],last_modified=_date)
                    else:
                        self.write({"status": False, "msg": 'Error', "value": ''})
                self.write({"status": True, "msg": 'successfully', "value": res})
            elif _act == 'export_pmd':
                data = self.get_argument("pmddata", None)
                data_js = json.loads(data)
                res = get_pmd(data_js)
                file_name = create_pmd_excel(res)
                if file_name:
                    path = os.path.join("static", "temp", file_name)
                    self.write({"status": True, "msg": 'successfully', "value": path})
                else:
                    self.write({"status": False, "msg": 'Error', "value": file_name})
            else:
                _code = self.get_argument("code", None)
                pmd_val = self.get_argument("pmd_val", None)
                rawcode = _code.replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-', '').replace(' ', '')
                pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
                if pr:
                    res = SysProducts(id=pr['id']).update(pmd=pmd_val)
                    self.write({"status": True, "msg": 'successfully', "value": res})
                else:
                    self.write({"status": False, "msg": 'Error', "value": ''})
        except Exception as e:
            print(e)
            self.write({"status": False, "msg": 'Error', "value": ''})


class SourceHandler(RequestHandler):
    def get(self):
        try:
            sourcels = SysSource().get_all()
            # sourcels = SysLogs().get_all_source_log_id()
            self.render("source.html", sourcels=sourcels)
        except Exception as e:
            print(e)

    def post(self):
        try:
            _act = self.get_argument("act", None)
            if _act == 'get_log':
                source_id = self.get_argument("source_id", None)
                # log = SysLogs(id=log_id).get_one()
                # if log:
                log_ls = SysLogs(source_id=source_id).get_all_by_source()
                self.write({"status": True, "msg": 'successfully', "value": log_ls})
                # else:
                #     self.write({"status": False, "msg": 'Error', "value": []})
            elif _act == 'delete_source':
                del_ls = []
                sourcels = json.loads(self.get_argument("sourcels", None))
                if sourcels:
                    sus = SysSource(id=sourcels).updatelist(status='delete')
                    if sus:
                        del_ls.append(dict(source_name=sourcels, status='delete'))
                    else:
                        del_ls.append(dict(source_name=sourcels, status='err_delete'))
                else:
                    del_ls.append(dict(source_name=sourcels, status='err_delete'))
                self.write({"status": True, "msg": 'successfully', "value": del_ls})
            elif _act == 'update_source':
                del_ls = []
                new_name = self.get_argument("new_name", None)
                if new_name.strip() != "":
                    sid = SysSource(name=new_name, description='---',status='publish').insert()
                    if sid:
                        sourcels = json.loads(self.get_argument("sourcels", None))
                        up_status = SysLogs().updatelist(ls=sourcels, source_id=sid)
                        if up_status:
                            sus = SysSource(id=sourcels).updatelist(status='delete')
                            if sus:
                                del_ls.append(dict(source_name=sourcels, status='delete'))
                            else:
                                del_ls.append(dict(source_name=sourcels, status='err_delete'))
                    else:
                        raise Exception("Error in insert new_name")
                else:
                    raise Exception("New Name Can't be Null")
                self.write({"status": True, "msg": 'successfully', "value": del_ls})
        except Exception as e:
            # print(e)
            self.write({"status": False, "msg": str(e), "value": []})
class ReviewHandler(RequestHandler):
    def get(self):
        try:
            self.render("review.html")
        except Exception as e:
            print(e)

    def post(self):
        try:
            _act = self.get_argument("request_number", None)

            if _act == 'get_log':
                source_id = self.get_argument("source_id", None)
                # log = SysLogs(id=log_id).get_one()
                # if log:
                log_ls = SysLogs(source_id=source_id).get_all_by_source()
                self.write({"status": True, "msg": 'successfully', "value": log_ls})
                # else:
                #     self.write({"status": False, "msg": 'Error', "value": []})
            elif _act == 'delete_source':
                del_ls = []
                sourcels = json.loads(self.get_argument("sourcels", None))
                if sourcels:
                    sus = SysSource(id=sourcels).updatelist(status='delete')
                    if sus:
                        del_ls.append(dict(source_name=sourcels, status='delete'))
                    else:
                        del_ls.append(dict(source_name=sourcels, status='err_delete'))
                else:
                    del_ls.append(dict(source_name=sourcels, status='err_delete'))
                self.write({"status": True, "msg": 'successfully', "value": del_ls})
            elif _act == 'update_source':
                del_ls = []
                new_name = self.get_argument("new_name", None)
                if new_name.strip() != "":
                    sid = SysSource(name=new_name, description='---',status='publish').insert()
                    if sid:
                        sourcels = json.loads(self.get_argument("sourcels", None))
                        up_status = SysLogs().updatelist(ls=sourcels, source_id=sid)
                        if up_status:
                            sus = SysSource(id=sourcels).updatelist(status='delete')
                            if sus:
                                del_ls.append(dict(source_name=sourcels, status='delete'))
                            else:
                                del_ls.append(dict(source_name=sourcels, status='err_delete'))
                    else:
                        raise Exception("Error in insert new_name")
                else:
                    raise Exception("New Name Can't be Null")
                self.write({"status": True, "msg": 'successfully', "value": del_ls})
        except Exception as e:
            # print(e)
            self.write({"status": False, "msg": str(e), "value": []})
