#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tornado.web import RequestHandler


class IndexHandler(RequestHandler):
    def get(self):
        # self.write("Hello, world")
        self.render("index.html", cname="PNAP Co")


class APIIndexHandler(RequestHandler):
    def get(self):
        # self.write("Hello, world")
        self.finish(
            dict(status=True, value=dict(name="Omid"))
        )


