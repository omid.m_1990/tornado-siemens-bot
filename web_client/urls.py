#!/usr/bin/env python
# -*- coding: utf-8 -*-
from web_client.handlers.index import IndexHandler
from web_client.handlers.siemens import GetDataHandler, ProductInsertHandler, PriceDataHandler, \
    ProductCheckHandler, PmdHandler, SourceHandler, ProductShowHandler, DownloadHandler, ReviewHandler

urls_list = [
    (r"/", IndexHandler),

    # (r'^(?i)/api/siemens[/]?([\w+\-\=]+)?[/]?$', APISiemensHandler),
    # (r'/api/siemens/(data)', APISiemensHandler, None, "siemens_get_data"),

    (r'/get-data[/]?$', GetDataHandler),

    (r'/data/update[/]?$', GetDataHandler),

    (r'/data/review[/]?$', ReviewHandler),

    (r'^(?i)/product/show[/]?([\w\-\=]+)?[/]?', ProductShowHandler),
    # (r'/product/show', ProductShowHandler, None, 'show_products1'),
    # (r'^(?i)/product/([\w.])[/]?', DownloadHandler),
    # (r'/product/show/(request)', ProductShowHandler, None, 'show_products'),

    (r'/product/recent-price[/]?$', PriceDataHandler),
    (r'/product/check[/]?$', ProductCheckHandler),
    (r'/product/insert[/]?$', ProductInsertHandler),

    (r'/pmd/update[/]?$', PmdHandler),
    (r'/pmd/export[/]?$', PmdHandler),

    (r'/source[/]?$', SourceHandler),
    (r'/source/get-log[/]?$', SourceHandler),

]
