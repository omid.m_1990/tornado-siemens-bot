
function change_input_val(input, val) {
    AutoNumeric.getAutoNumericElement(input).set(val);
}

function sales_price_update(data_id) {
    var index = data_id-1;
    var customer_discount = AutoNumeric.getNumber('input[name=customer-discount-' + data_id + ']');
    var pprice = AutoNumeric.getNumber('input[name=purchase-price-' + data_id + ']');
    var sprice = parseInt(pprice * ((customer_discount + 100) / 100));
    AutoNumeric.getAutoNumericElement('input[name=sales-price-' + data_id + ']').set(sprice);
    form_state_update(index,"Sales_price", sprice);
    $('input[name=sales-price-' + data_id + ']').addClass("edited");
    sum_price_update(data_id);
}

function customer_discount_update(data_id) {
    var index = data_id-1;
    var pprice = AutoNumeric.getNumber('input[name=purchase-price-' + data_id + ']');
    var sprice = AutoNumeric.getNumber('input[name=sales-price-' + data_id + ']');
    var cdiscount = ((sprice * 1.0) / pprice);
    cdiscount = (cdiscount - 1) * 100;
    console.log("Discount : ", cdiscount);
    AutoNumeric.getAutoNumericElement('input[name=customer-discount-' + data_id + ']').set(cdiscount);
    form_state_update(index,"customertdiscount", cdiscount);
    $('input[name=customer-discount-' + data_id + ']').addClass("edited");
    sum_price_update(data_id);
}

function purchase_price_update(data_id, ratio) {
    var index = data_id-1;
    var euro = AutoNumeric.getNumber('input[name=euro]');
    var pmd = AutoNumeric.getNumber('input[name=pmd-' + data_id + ']');
    var pprice = parseInt(euro * pmd * ratio);
    console.log("pprice :",pprice);
    AutoNumeric.getAutoNumericElement('input[name=purchase-price-' + data_id + ']').set(pprice);
    form_state_update(index,"Purchase_price", pprice);
    $('input[name=purchase-price-' + data_id + ']').addClass("edited");
    sum_price_update(data_id);
}

function pmd_ratio_update(data_id) {
    var pprice = AutoNumeric.getNumber('input[name=purchase-price-' + data_id + ']');
    var euro = AutoNumeric.getNumber('input[name=euro]');
    var pmd = AutoNumeric.getNumber('input[name=pmd-' + data_id + ']');
    if (pmd * euro) {
        var ratio = (pprice / (euro * pmd));
        $('input[name=pmd-ratio-' + data_id + ']').val(ratio).addClass("edited");
    }
}

function form_state_update(index, key, val) {
    // alert(index);
    // alert(key);
    // alert(val);
    form_state[index][key] = val;
    console.log(key + " changed :", index, form_state[index][key]);
}

function sum_price_update(data_id) {
    var pp = form_state[data_id-1]['Purchase_price'];
    var sp = form_state[data_id-1]['Sales_price'];
    var qty = form_state[data_id-1]['qty'];
    var profit = sp-pp;
    var sumprofit = profit*qty;
    // alert(sumprofit);
    // alert(profit);
    new AutoNumeric('.sumprofit-'+data_id).set(sumprofit);
    new AutoNumeric('.profit-'+data_id).set(profit);

}
function dt_check_update_class(data_id) {
    var dt = $('input[name=dt-pmd-ratio-' + data_id + ']').val();
    var pmd = AutoNumeric.getNumber('input[name=pmd-' + data_id + ']');
    var euro = $('input[name=euro]').val();
    if (dt && pmd && euro){
        $('input[name=dt-pmd-ratio-check-' + data_id + ']').addClass("enable_dt_check");
    }
    else {
        $('input[name=dt-pmd-ratio-check-' + data_id + ']').removeClass("enable_dt_check");
    }
}

