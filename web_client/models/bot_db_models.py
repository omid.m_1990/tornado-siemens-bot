# !/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime

from khayyam import *
from peewee import *

from configs import Configs

# # Connect to a MySQL database on network.
# mysql_db = MySQLDatabase('pnap_bot_db', user='root', password='',
#                          host='localhost', port=3306, charset="utf8")


#
# mysql_db = MySQLDatabase('pnap_bot_db', user='bot_user', password='QCyZ9CZ2$*CYsNTg&Sb',
#                          host='localhost', port=3306, charset="utf8")
mysql_db = Configs.mysql_db


# model definitions -- the standard "pattern" is to define a base model class
# that specifies which database to use.  then, any subclasses will automatically
# use the correct storage.
class BaseModel(Model):
    class Meta:
        database = mysql_db


# the user model specifies its fields (or columns) declaratively, like django
class Users(BaseModel):
    id = IntegerField()
    username = CharField(unique=True)
    f_name = CharField()
    start_date = DateTimeField()
    status = CharField()


class Static_data(BaseModel):
    id = IntegerField()
    name = CharField(unique=True)
    value = CharField()
    date = DateTimeField()


class Products(BaseModel):
    id = IntegerField()
    order_number = CharField()
    part_name = CharField()
    description = CharField()
    image = TextField()
    datasheet = TextField()
    rawcode = CharField()
    last_modified = DateTimeField()
    status = CharField()
    brand = CharField()
    brand_group = CharField()
    category = CharField()
    pmd = FloatField()
    replace_code = TextField()
    coo = CharField()
    lifecycle = CharField()
    weight = FloatField()
    notes = TextField()


class Attribute_products(BaseModel):
    id = IntegerField()
    p = ForeignKeyField(Products, to_field=Products.id)
    attribute_name = CharField()
    attribute_value = CharField()
    last_modified = DateTimeField()
    status = CharField()
    sort_order = IntegerField()


class Pmd_ratio(BaseModel):
    id = IntegerField()
    root_name = CharField()
    value = FloatField()
    last_modified = DateTimeField()

class Source(BaseModel):
    id = IntegerField()
    name = CharField()
    description = TextField()
    last_modified = DateTimeField()
    status = CharField()

class Logs(BaseModel):
    id = IntegerField()
    u_id = ForeignKeyField(Users, to_field=Users.id, )
    p_id = ForeignKeyField(Products, to_field=Products.id)
    request_date = DateTimeField()
    price = CharField()
    source_id = ForeignKeyField(Source, to_field=Source.id)
    status = CharField()
    request_number = CharField()
    currency = CharField()
    euro = FloatField()
    doller = FloatField()
    customer = CharField()


class SysUsers:
    def __init__(self, id=None, username=None, f_name=None, start_date=None, status=None):
        self.id = id
        self.username = username
        self.f_name = f_name
        self.start_date = start_date
        self.status = status

    def insert(self):
        try:
            x = Users.insert(
                id=self.id,
                username=self.username,
                f_name=self.f_name,
                # start_date=self.start_date,
                status=self.status
            ).execute()
            return x if x else False
        except Exception as e:
            print(e)
            return False

    def get_one(self):
        try:
            ex = Users.id == self.id
            i = Users.get(ex)
            return dict(
                id=i.id,
                username=i.username,
                f_name=i.f_name,
                start_date=i.start_date,
                status=i.status
            )
        except Exception as e:
            print(e)
            return dict()

    def delete(self):
        try:
            x = Users.delete().where(Users.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def update(self, **kwargs):
        try:
            x = Users.update(**kwargs).where(Users.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False


class SysStaticData:
    def __init__(self, id=None, name=None, value=None, date=None):
        self.id = id
        self.name = name
        self.value = value
        self.date = date

    def insert(self):
        try:
            x = Static_data.insert(
                name=self.name,
                value=self.value
            ).execute()
            return x if x else False
        except Exception as e:
            print(e)
            return False

    def get_one(self):
        try:
            ex = Static_data.id == self.id
            i = Static_data.get(ex)
            return dict(
                id=i.id,
                name=i.name,
                value=i.value,
                date=i.date
            )
        except Exception as e:
            print(e)
            return dict()

    def get_one_by_name(self):
        try:
            ex = Static_data.name == self.name
            i = Static_data.get(ex)
            return dict(
                id=i.id,
                name=i.name,
                value=i.value,
                date=i.date
            )
        except Exception as e:
            print(e)
            return dict()

    def delete(self):
        try:
            x = Static_data.delete().where(Static_data.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def update(self, **kwargs):
        try:
            x = Static_data.update(**kwargs).where(Static_data.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False


class SysProducts:
    def __init__(self, id=None, order_number=None, part_name=None, description=None, image=None, datasheet=None,
                 rawcode=None, last_modified=None, status=None, brand=None, brand_group=None, category=None, pmd=None,
                 replace_code=None, coo=None, lifecycle=None, weight=None, notes=None):
        self.id = id
        self.order_number = order_number
        self.part_name = part_name
        self.description = description
        self.image = image
        self.datasheet = datasheet
        self.rawcode = rawcode
        self.status = status
        self.brand = brand
        self.brand_group = brand_group
        self.last_modified = last_modified
        self.category = category
        self.pmd = pmd
        self.replace_code = replace_code
        self.coo= coo
        self.lifecycle= lifecycle
        self.weight= weight
        self.notes= notes

    def insert(self):
        try:
            x = Products.insert(
                id=self.id,
                order_number=self.order_number,
                part_name=self.part_name,
                description=self.description,
                image=self.image,
                datasheet=self.datasheet,
                rawcode=self.rawcode,
                status=self.status,
                brand=self.brand,
                brand_group=self.brand_group,
                category=self.category,
                pmd=self.pmd,
                replace_code=self.replace_code,
                coo=self.coo,
                lifecycle =self.lifecycle,
                weight =self.weight,
                notes =self.notes

            ).execute()
            return x if x else False
        except Exception as e:
            print(e)
            return False

    def get_one(self):
        try:
            i = Products.get(Products.id == self.id)
            return dict(
                id=i.id,
                order_number=i.order_number,
                part_name=i.part_name,
                description=i.description,
                image_url=i.image,
                datasheet=i.datasheet,
                rawcode=i.rawcode,
                last_modified=i.last_modified,
                status=i.status,
                brand=i.brand,
                brand_group=i.brand_group,
                category=i.category,
                pmd=i.pmd,
                replace_code=i.replace_code,
                coo=i.coo,
                Lifecycle=i.lifecycle,
                weight=i.weight,
                Notes=i.notes
            )
        except Exception as e:
            print(e)
            return dict()

    def get_one_by_rawcode(self):
        try:
            i = Products.get(Products.rawcode == self.rawcode)

            return dict(
                id=i.id,
                order_number=i.order_number,
                part_name=i.part_name if i.part_name else i.order_number,
                description=i.description,
                image_url=i.image if 'http' in i.image else 'https://pnap.ir' + i.image,
                datasheet=i.datasheet,
                rawcode=i.rawcode,
                last_modified=JalaliDatetime(i.last_modified).strftime("%Y/%m/%d - %H:%M"),
                status=i.status,
                brand=i.brand,
                brand_group=i.brand_group,
                category=i.category,
                pmd=i.pmd,
                replace_code=i.replace_code,
                coo=i.coo,
                Lifecycle=i.lifecycle,
                weight=i.weight,
                Notes=i.notes
            )
        except Exception as e:
            print(e)
            return dict()

    def get_all(self, len):
        try:
            x = Products.select().where(Products.status=='publish').limit(len)
            # print(x)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        order_number=i.order_number,
                        part_name=i.part_name,
                        description=i.description,
                        image_url=i.image,
                        datasheet=i.datasheet,
                        rawcode=i.rawcode,
                        last_modified=str(i.last_modified),
                        status=i.status,
                        brand=i.brand,
                        brand_group=i.brand_group,
                        category=i.category,
                        pmd=i.pmd,
                        replace_code=i.replace_code,
                        coo=i.coo,
                        Lifecycle=i.lifecycle,
                        weight=i.weight,
                        Notes=i.notes
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_by_rawcode(self):
        try:
            x = Products.select().order_by(Products.rawcode).where(
                Products.rawcode.contains(self.rawcode) | Products.order_number.contains(self.rawcode)).limit(15)
            # print(x)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        order_number=i.order_number,
                        part_name=i.part_name,
                        description=i.description,
                        image_url=i.image,
                        datasheet=i.datasheet,
                        rawcode=i.rawcode,
                        last_modified=str(i.last_modified),
                        status=i.status,
                        brand=i.brand,
                        brand_group=i.brand_group,
                        category=i.category,
                        pmd=i.pmd,
                        replace_code=i.replace_code,
                        coo=i.coo,
                        Lifecycle=i.lifecycle,
                        weight=i.weight,
                        Notes=i.notes
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []
    def get_all_no_pmd(self, _len):
        try:
            x = Products.select().where(Products.pmd<=1).limit(_len)
            # print(x)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        order_number=i.order_number,
                        part_name=i.part_name,
                        description=i.description,
                        image_url=i.image,
                        datasheet=i.datasheet,
                        rawcode=i.rawcode,
                        last_modified=str(i.last_modified),
                        status=i.status,
                        brand=i.brand,
                        brand_group=i.brand_group,
                        category=i.category,
                        pmd=i.pmd,
                        replace_code=i.replace_code,
                        coo=i.coo,
                        Lifecycle=i.lifecycle,
                        weight=i.weight,
                        Notes=i.notes
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_valid_pmd(self, _len):
        try:
            x = Products.select().where(Products.pmd>1).limit(_len)
            # print(x)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        order_number=i.order_number,
                        part_name=i.part_name,
                        description=i.description,
                        image_url=i.image,
                        datasheet=i.datasheet,
                        rawcode=i.rawcode,
                        last_modified=str(i.last_modified),
                        status=i.status,
                        brand=i.brand,
                        brand_group=i.brand_group,
                        category=i.category,
                        pmd=i.pmd,
                        replace_code=i.replace_code,
                        coo=i.coo,
                        Lifecycle=i.lifecycle,
                        weight=i.weight,
                        Notes=i.notes
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []



    def delete(self):
        try:
            x = Products.delete().where(Products.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def update(self, **kwargs):
        try:
            x = Products.update(**kwargs).where(Products.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False


class SysLogs:
    def __init__(self, id=None, u_id=None, p_id=None, request_date=None, price=None, source_id=None, status='valid',
                 request_number=None, currency=None, euro=None, doller=None, customer=None):
        self.id = id
        self.u_id = u_id
        self.p_id = p_id
        self.request_date = request_date
        self.price = price
        self.source_id = source_id
        self.status = status
        self.request_number = request_number
        self.currency = currency
        self.euro = euro
        self.doller = doller
        self.customer = customer

    def insert(self):
        try:
            x = Logs.insert(
                id=self.id,
                u_id=self.u_id,
                p_id=self.p_id,
                # request_date=self.request_date,
                price=self.price,
                source_id=self.source_id,
                request_number=self.request_number,
                currency=self.currency,
                euro=self.euro,
                doller=self.doller,
                customer=self.customer
            ).execute()
            return x if x else False
        except Exception as e:
            print(e)
            return False

    def get_one(self):
        try:
            ex = Logs.id == self.id
            i = Logs.get(ex)
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M")),
                price=i.price,
                source_id=i.source_id.name,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                customer=i.customer
            )
        except Exception as e:
            print(e)
            return dict()

    def get_all_by_pid(self):
        try:
            x = Logs.select().where(Logs.p_id == self.p_id)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number,
                        request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M"),
                        price=i.price,
                        source_id=i.source_id.name,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller,
                        customer=i.customer
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_source_product(self):
        try:
            x = Logs.select().join(Source, on=(Logs.source_id == Source.id)).where(Logs.p_id == self.p_id, Logs.status == "valid",Source.status == 'publish').group_by(Logs.source_id)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        source=i.source_id.name,
                        source_id=i.source_id.id
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_source(self):
        try:
            x = Logs.select().where(Logs.status == "valid").group_by(Logs.source_id)
            ls = []
            for i in x:
                ls.append(i.source_id.name)
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_source_log_id(self):
        try:
            x = Logs.select().where(Logs.status == "valid").group_by(Logs.source_id)
            ls = []
            for i in x:
                ls.append(dict(name=i.source_id.name, log_id=i.id))
            return ls
        except Exception as e:
            print(e)
            return []

    def get_10last_by_pid(self):
        try:
            x = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid').order_by(Logs.id.desc()).paginate(1, 10)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number,
                        request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M")),
                        price=i.price,
                        source_id=i.source_id.name,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller,
                        customer=i.customer
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_10last_by_pid_source(self):
        try:
            x = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.price,
                                    Logs.source_id == self.source_id).order_by(Logs.id.desc()).paginate(1, 10)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number,
                        request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M")),
                        price=i.price,
                        source=i.source_id.name,
                        source_id=i.source_id.id,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller,
                        customer=i.customer
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_by_date(self):
        try:
            x = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.price,
                                    Logs.request_date > datetime.datetime.now() - datetime.timedelta(days=30))
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number,
                        request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M"),
                        price=i.price,
                        source_id=i.source_id.name,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller,
                        customer=i.customer
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_by_user(self):
        try:
            x = Logs.select(fn.COUNT(Logs.p_id), Logs).group_by(Logs.p_id).where(Logs.status == 'valid', Logs.price,
                                                                                 Logs.u_id == self.u_id)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number.replace('-', '_').replace(' ', '_'),
                        image_url=i.p_id.image,
                        request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M")),
                        price=i.price,
                        source_id=i.source_id.name,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller,
                        customer=i.customer
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_by_source(self):
        try:
            x = Logs.select().where(Logs.status == 'valid', Logs.price,Logs.source_id == self.source_id)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number.replace('-', '_').replace(' ', '_'),
                        image_url=i.p_id.image,
                        request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M")),
                        price=i.price,
                        source_id=i.source_id.name,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller,
                        customer=i.customer
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []


    def get_all_by_user_and_date(self):
        try:
            x = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.u_id == self.u_id, Logs.price,
                                    Logs.request_date > datetime.datetime.now() - datetime.timedelta(days=30))
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number,
                        request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M"),
                        price=i.price,
                        source_id=i.source_id.name,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller,
                        customer=i.customer
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_last_by_user(self):
        try:
            i = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.price,
                                    Logs.u_id == self.u_id).order_by(
                Logs.id.desc()).get()
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d %H:%M"),
                price=i.price,
                source_id=i.source_id.name,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                customer=i.customer
            )
        except Exception as e:
            print(e)
            return {}


    def get_last(self):
        try:
            i = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.price,
                                    Logs.source_id != "db").order_by(Logs.id.desc()).get()
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d %H:%M")),
                price=i.price,
                source=i.source_id.name,
                source_id=i.source_id.id,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                customer=i.customer
            )
        except Exception as e:
            print(e)
            return {}

    def get_max_price(self, days=530):
        try:
            i = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.source_id != "db", Logs.price,
                                    Logs.request_date > datetime.datetime.now() - datetime.timedelta(days=days)
                                    ).order_by(Logs.price.desc()).get()
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d %H:%M")),
                price=i.price,
                source=i.source_id.name,
                source_id=i.source_id.id,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                customer=i.customer
            )
        except Exception as e:
            print(e)
            return {}

    def get_max_price_by_source(self, days=530):
        try:
            i = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.source_id == self.source_id,
                                    Logs.price,
                                    Logs.request_date > datetime.datetime.now() - datetime.timedelta(days=days)
                                    ).order_by(Logs.price.desc()).get()
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d %H:%M"),
                price=i.price,
                source=i.source_id.name,
                source_id=i.source_id.id,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                customer=i.customer
            )
        except Exception as e:
            print(e)
            return {}

    def get_last_by_source(self):
        try:
            i = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.price,
                                    Logs.source_id == self.source_id
                                    ).order_by(Logs.id.desc()).get()
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d %H:%M"),
                price=i.price,
                source_id=i.source_id.name,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                customer=i.customer
            )
        except Exception as e:
            print(e)
            return {}

    def get_min_price(self, days=530):
        try:
            i = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.price, Logs.source_id != "db",
                                    Logs.request_date > datetime.datetime.now() - datetime.timedelta(days=days)
                                    ).order_by(Logs.price.asc()).get()
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=str(JalaliDatetime(i.request_date).strftime("%Y/%m/%d %H:%M")),
                price=i.price,
                source=i.source_id.name,
                source_id=i.source_id.id,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                customer=i.customer
            )
        except Exception as e:
            print(e)
            return {}

    def get_min_price_by_source(self, days=530):
        try:
            i = Logs.select().where(Logs.p_id == self.p_id, Logs.status == 'valid', Logs.price,
                                    Logs.source_id == self.source_id,
                                    Logs.request_date > datetime.datetime.now() - datetime.timedelta(days=days)
                                    ).order_by(Logs.price.asc()).get()
            return dict(
                id=i.id,
                u_id=i.u_id.username,
                u_name=i.u_id.f_name,
                p_id=i.p_id.order_number,
                request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d %H:%M"),
                price=i.price,
                source=i.source_id.name,
                source_id=i.source_id.id,
                status=i.status,
                request_number=i.request_number,
                currency=i.currency,
                euro=i.euro,
                doller=i.doller,
                        customer=i.customer
            )
        except Exception as e:
            print(e)
            return {}

    def get_all_by_request_number(self):
        try:
            x = Logs.select().where(Logs.request_number == self.request_number)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        u_id=i.u_id.username,
                        u_name=i.u_id.f_name,
                        p_id=i.p_id.order_number,
                        request_date=JalaliDatetime(i.request_date).strftime("%Y/%m/%d - %H:%M"),
                        price=i.price,
                        source_id=i.source_id.name,
                        status=i.status,
                        request_number=i.request_number,
                        currency=i.currency,
                        euro=i.euro,
                        doller=i.doller
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def update(self, **kwargs):
        try:
            x = Logs.update(**kwargs).where(Logs.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def updatelist(self,ls, **kwargs):
        try:
            x = Logs.update(**kwargs).where(Logs.source_id << ls).execute()
            return True
        except Exception as e:
            print(e)
            return False



class SysAttributeProducts:
    def __init__(self, id=None, p_id=None, attribute_name=None, attribute_value=None, last_modified=None,
                 status='publish',
                 sort_order=None):
        self.id = id
        self.p = p_id
        self.attribute_name = attribute_name
        self.attribute_value = attribute_value
        self.last_modified = last_modified
        self.status = status
        self.sort_order = sort_order

    def insert(self):
        try:
            x = Attribute_products.insert(
                p=self.p,
                attribute_name=self.attribute_name,
                attribute_value=self.attribute_value,
                status=self.status,
                sort_order=self.sort_order
            ).execute()
            return x if x else False
        except Exception as e:
            print(e)
            return False

    def get_all(self):
        try:
            x = Attribute_products.select()
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        p_id=i.p_id,
                        attribute_name=i.attribute_name,
                        # attribute_value=i.attribute_value,
                        # last_modified=i.last_modified,
                        status=i.status,
                        # sort_order=i.sort_order
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_all_by_pid(self):
        try:
            x = Attribute_products.select().where(Attribute_products.p == self.p,
                                                  Attribute_products.status == self.status)
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        p_id=i.p_id,
                        attribute_name=i.attribute_name,
                        attribute_value=i.attribute_value,
                        last_modified=i.last_modified,
                        status=i.status,
                        sort_order=i.sort_order
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_product_attr(self):
        try:
            i = Attribute_products.get(Attribute_products.p == self.p,
                                       Attribute_products.status == 'publish',
                                       Attribute_products.attribute_name.contains(self.attribute_name))
            return dict(
                id=i.id,
                p_id=i.p_id,
                attribute_name=i.attribute_name,
                attribute_value=i.attribute_value,
                last_modified=i.last_modified,
                status=i.status,
                sort_order=i.sort_order
            )
        except Exception as e:
            # print(e)
            return {}

    def update(self, **kwargs):
        try:
            x = Attribute_products.update(**kwargs).where(Attribute_products.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def update_by_attr_pid(self, **kwargs):
        try:
            x = Attribute_products.update(**kwargs).where(Attribute_products.p == self.p,
                                                          Attribute_products.attribute_name == self.attribute_name).execute()
            return True
        except Exception as e:
            print(e)
            return False


class SysPmdRatio:
    def __init__(self, id=None, root_name=None, value=None, last_modified=None):
        self.id = id
        self.root_name = root_name
        self.value = value
        self.last_modified = last_modified

    def insert(self):
        try:
            x = Pmd_ratio.insert(
                id=self.id,
                root_name=self.root_name,
                value=self.value
            ).execute()
            return x if x else False
        except Exception as e:
            print(e)
            return False

    def get_one(self):
        try:
            ex = Pmd_ratio.id == self.id
            i = Pmd_ratio.get(ex)
            return dict(
                id=i.id,
                root_name=i.root_name,
                value=i.value,
                last_modified=i.last_modified
            )
        except Exception as e:
            print(e)
            return dict()

    def get_one_by_code(self):
        try:
            ex = Pmd_ratio.root_name == self.root_name
            i = Pmd_ratio.get(ex)
            return dict(
                id=i.id,
                root_name=i.root_name,
                value=i.value,
                last_modified=i.last_modified
            )
        except Exception as e:
            # print(e)
            return dict()

    def delete(self):
        try:
            x = Pmd_ratio.delete().where(Pmd_ratio.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def update(self, **kwargs):
        try:
            x = Pmd_ratio.update(**kwargs).where(Pmd_ratio.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False
class SysSource:
    def __init__(self, id=None, name=None, description=None, last_modified=None, status=None):
        self.id = id
        self.name = name
        self.description = description
        self.last_modified = last_modified
        self.status= status


    def insert(self):
        try:
            x = Source.insert(
                name=self.name,
                description=self.description,
                status=self.status
            ).execute()
            return x if x else False
        except Exception as e:
            print(e)
            return False

    def get_one(self):
        try:
            ex = Source.id == self.id
            i = Source.get(ex)
            return dict(
                id=i.id,
                name=i.name,
                description=i.description,
                last_modified=i.last_modified,
                status=i.status
            )
        except Exception as e:
            print(e)
            return dict()

    def get_all(self):
        try:
            x = Source.select().where(Source.status == 'publish')
            ls = []
            for i in x:
                ls.append(
                    dict(
                        id=i.id,
                        name=i.name,
                        description=i.description,
                        last_modified = i.last_modified,
                        status=i.status
                    )
                )
            return ls
        except Exception as e:
            print(e)
            return []

    def get_one_by_name(self):
        try:
            # ex = (Source.name == self.name) , (Source.status == 'publish')
            # ex = ()
            i = Source.get(Source.name == self.name, Source.status !='delete')
            return dict(
                id=i.id,
                name=i.name,
                description=i.description,
                last_modified=i.last_modified,
                status=i.status
            )
        except Exception as e:
            # print(e)
            return dict()

    def delete(self):
        try:
            x = Source.delete().where(Source.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def update(self, **kwargs):
        try:
            x = Source.update(**kwargs).where(Source.id == self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False

    def updatelist(self, **kwargs):
        try:
            x = Source.update(**kwargs).where(Source.id << self.id).execute()
            return True
        except Exception as e:
            print(e)
            return False


