# # !/usr/bin/env python
# # -*- coding: utf-8 -*-
# import datetime
# import random
# import re
# from collections import OrderedDict
#
# import requests
# from bs4 import BeautifulSoup
# from khayyam import JalaliDatetime
#
# from web_client.models.bot_db_models import SysLogs, SysProducts
#
#
# def tracking_code():
#     try:
#         dt = JalaliDatetime(datetime.datetime.now())
#         d = dt.strftime("%Y%m%d")[2:]
#         t = random.randint(101, 999)
#         request_number = '#N{}_{}'.format(d, t)
#         # while SysLogs(request_number=request_number).get_all_by_request_number():
#         #     t = random.randint(101, 999)
#         #     request_number = '#N{}_{}'.format(d, t)
#     except Exception as e:
#         request_number = False
#
#     return request_number
#
#
# def vip_get_data(ls, parameter):
#     js = []
#     err = []
#
#     try:
#         for i in ls:
#             res = False
#             item = dict(
#                 code='',
#                 description=False,
#                 standard_ptn=False,
#                 weight=False,
#                 coo=False,
#                 Lifecycle=False,
#                 Notes=False,
#                 datasheet=False,
#                 image_url=False,
#                 replace=False,
#                 status=True,
#                 pmd=1,
#                 # last_modified=str(JalaliDatetime(datetime.datetime.now()).strftime("%Y/%m/%d - %H:%M")),
#
#                 qty=1,
#                 price=0,
#                 currency='IRR',
#                 mydiscount=float(parameter['my_ratio']) if parameter['my_ratio'] else 1,
#                 customertdiscount=float(parameter['customer_ratio']) if parameter['customer_ratio'] else 1,
#                 Purchase_price=0,
#                 Sales_price=0,
#                 profit=0,
#                 Statement=parameter['statement'],
#                 deliver=parameter['delivery'],
#                 source=parameter['source'],
#                 msg={"msg": "", "from": ""},
#                 user="",
#                 sourcels=[],
#                 euro=parameter['euro'] if parameter['euro'] else 0
#             )
#
#             try:
#                 source = i.split('&', 1)
#                 item['source'] = source[1].strip()
#             except Exception as er:
#                 pass
#
#             try:
#                 deliver = source[0].split('~', 1)
#                 item['deliver'] = deliver[1]
#             except Exception as er:
#                 pass
#
#             statement = deliver[0].split('!')
#             try:
#                 item['Statement'] = statement[1]
#             except Exception as er:
#                 pass
#
#             ii = statement[0].split('=', 1)
#             iii = ii[0].split('/')
#             item['code'] = iii[0].strip()
#             if len(iii) > 1:
#                 if iii[1].strip() != '':
#                     item['qty'] = iii[1].strip()
#
#             rawcode = item['code'].replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-', '').replace(' ', '')
#             pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
#             if len(ii) > 1:
#                 if ii[1].strip() != '':
#                     iii2 = ii[1].split('*')
#                     price = iii2[0].strip()
#                     try:
#                         prc = ''.join(filter(str.isdigit, price[0]))
#                         if not prc:
#                             try:
#                                 # rawcode = item['code'
#                                 # ].replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-', '').replace(' ', '')
#                                 # pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
#                                 if pr:
#                                     item['status'] = True
#                                     item.update(pr)
#                                     if 'last' in price:
#                                         try:
#                                             ex = re.findall(r"[(](\w+)[)]", price)[0]
#                                             res = SysLogs(p_id=pr['id'], source=ex).get_last_by_source()
#                                             # delta = ''.join(filter(str.isdigit, ex))
#                                         except Exception:
#                                             res = SysLogs(p_id=pr['id']).get_last()
#                                     elif 'max' in price:
#                                         try:
#                                             ex = re.findall(r"[(](\w+)[)]", price)[0]
#                                             delta = ''.join(filter(str.isdigit, ex))
#                                             if delta:
#                                                 res = SysLogs(p_id=pr['id']).get_max_price(int(delta))
#                                             else:
#                                                 res = SysLogs(p_id=pr['id'], source=ex).get_max_price_by_source()
#                                         except Exception as ett:
#                                             pass
#                                         if not res:
#                                             res = SysLogs(p_id=pr['id']).get_max_price()
#                                     elif 'min' in price:
#                                         try:
#                                             ex = re.findall(r"[(](\w+)[)]", price)[0]
#                                             delta = ''.join(filter(str.isdigit, ex))
#                                             if delta:
#                                                 res = SysLogs(p_id=pr['id']).get_min_price(int(delta))
#                                             else:
#                                                 res = SysLogs(p_id=pr['id'], source=ex).get_min_price_by_source()
#                                         except Exception:
#                                             pass
#                                         if not res:
#                                             res = SysLogs(p_id=pr['id']).get_min_price()
#                                     else:
#                                         item['pmd'] = pr['pmd']
#                                         res = SysLogs(p_id=pr['id'], source=price).get_last_by_source()
#                                     if res:
#                                         item['price'] = price = int(res['price'])
#                                         item['date'] = str(res['request_date'])
#                                         item['user'] = str(res['u_id'])
#                                         item['source'] = str(res['source'])
#                                         item['msg']['from'] = "db"
#                                         item['pmd'] = pr['pmd']
#                                         item['euro'] = res['euro']
#                                         item['doller'] = res['doller']
#                                     else:
#                                         item['msg']['msg'] = "Log Not Found (1)"
#                                 else:
#                                     item['msg']['msg'] = "Log Not Found (2)"
#                                     siemens_request([item],True)
#                             except Exception as erer:
#                                 print(erer)
#                         else:
#                             try:
#                                 # rawcode = item['code'
#                                 # ].replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-', '').replace(' ', '')
#                                 # pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
#                                 if pr:
#                                     item.update(pr)
#                                     # item['pmd'] = pr['pmd']
#                                     item['currency'] = price.split(''.join(filter(str.isdigit, price))[-1])[-1].strip()
#                                 else:
#                                     siemens_request([item],True)
#                             except Exception as ere:
#                                 print(ere)
#                                 item['currency'] = "IRR"
#                         try:
#                             strprice = ''.join(filter(str.isdigit, str(price)))
#                             if strprice != '':
#                                 item['price'] = int(strprice)
#                             if len(iii2) > 1:
#                                 item['mydiscount'] = float(iii2[1])
#                             if len(iii2) > 2:
#                                 item['customertdiscount'] = float(iii2[2])
#
#                         except Exception as ee:
#                             err.append('Error input format in {}'.format(i))
#                     except Exception as errrr:
#                         pass
#                     if float(item['mydiscount']) != 1:
#                         item['Purchase_price'] = int((item['mydiscount'] * int(item['price'])) / 10000) * 10000
#
#                         # --------------------------------------------------------
#                         item['mydiscount']= round(float(item['mydiscount']-1)*100)
#                         # --------------------------------------------------------
#
#                     else:
#                         item['Purchase_price'] = item['price']
#                     if float(item['customertdiscount']) != 1:
#                         salesP = (item['Purchase_price']) * float(item['customertdiscount'])
#
#                         # -----------------------------------------------------------------
#                         item['customertdiscount'] = round(float(item['customertdiscount']-1)*100)
#                         # -----------------------------------------------------------------
#
#
#                         if salesP > 10000:
#                             rond = salesP % 10000
#                             salesP = salesP // 10000
#                             if rond < 5000:
#                                 item['Sales_price'] = int(salesP * 10000)
#                             else:
#                                 item['Sales_price'] = int((salesP + 1) * 10000)
#                         elif salesP > 1000:
#                             rond = salesP % 1000
#                             salesP = salesP // 1000
#                             if rond < 500:
#                                 item['Sales_price'] = int(salesP * 1000)
#                             else:
#                                 item['Sales_price'] = int((salesP + 1) * 1000)
#                         else:
#                             item['Sales_price'] = round(salesP)
#                     else:
#                         item['Sales_price'] = item['Purchase_price']
#                     item['profit'] = (item['Sales_price'] - item['Purchase_price']) * int(item['qty'])
#                 else:
#                     pass
#             else:
#                 pass
#             js.append(item)
#     except Exception as e:
#         print('###' + str(e))
#     return js
#
#
# def siemens_request(js, save):
#     agent = {
#         "User-Agent": 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
#     }
#     try:
#         for i in js:
#             try:
#                 req = ''
#                 replace = False
#                 url = 'https://mall.industry.siemens.com/mall/en/WW/Catalog/Product/' + i['code']
#                 i['url'] = url
#                 print(url)
#                 req = requests.get(url, headers=agent)
#                 req = req.text
#                 bs = BeautifulSoup(req, "html.parser")
#                 # print(bs)
#
#                 try:
#                     # ----------------get description ------
#                     description = bs.find("table", {"class": "ProductDetailsTable"})
#                     trs = description.find_all("tr")
#                     description = trs[2]
#                     description = description.find_all("td")
#                     i['description'] = description[1].text.strip()
#                     # ----------------get order number
#                     prt_number = bs.find("span", {"class": 'productIdentifier'}).text.strip()
#                     i['standard_ptn'] = prt_number
#
#                     try:
#                         i['image_url'] = bs.find("img", {"class": "productPicture"})['src']
#                     except Exception as e:
#                         i['image_url'] = 'https://pnap.ir/wp-content/uploads/public/no_image_pnap600.jpg'
#
#                     try:
#                         datasheet = bs.find("a", {"class": "pdfLink"})['href']
#                         i['datasheet'] = 'https://mall.industry.siemens.com{}'.format(datasheet)
#                     except Exception as e:
#                         print(e, "datasheet except")
#                         i['datasheet'] = url
#
#                     try:
#                         replace = bs.find("div", {"class": "bluelinesBox"})
#                         if replace:
#                             replace_link = replace.find("a", {"class": "internalLink"})
#                             try:
#                                 replace = replace_link.text
#                                 i['replace'] = [dict(
#                                     code=replace,
#                                     pmd=1,
#                                     description=False,
#                                     standard_ptn=False,
#                                     weight=0,
#                                     coo=False,
#                                     Lifecycle=False,
#                                     Notes=False,
#                                     datasheet=False,
#                                     image_url='https://pnap.ir/wp-content/uploads/public/no_image_pnap600.jpg',
#                                     replace=False,
#                                     status=True,
#                                     # last_modified=str(
#                                     #     JalaliDatetime(datetime.datetime.now()).strftime("%Y/%m/%d - %H:%M")),
#
#                                     qty=1,
#                                     price=0,
#                                     currency='IRR',
#                                     mydiscount=1,
#                                     customertdiscount=1,
#                                     Purchase_price=0,
#                                     Sales_price=0,
#                                     profit=0,
#                                     Statement=False,
#                                     deliver=False,
#                                     source=False,
#                                     msg={"msg": "", "from": ""},
#                                     user="",
#                                     sourcels=[],
#                                     euro=0
#                                 )]
#
#                                 siemens_request(i['replace'],True)
#
#                             except Exception as e_replace:
#                                 replace = replace.text
#
#                     except Exception as e:
#                         pass
#                     # for w in trs:
#                         # td = w.find_all('td')
#                         # if td[0].text == 'Net Weight (kg)':
#                         #     i['weight'] = td[1].text.strip()
#                         #     i['weight'] = i['weight'].split(' ')
#                         #     i['weight'][0] = i['weight'][0].replace(',', '.')
#                         # elif td[0].text == 'Country of origin':
#                         #     i['coo'] = td[1].text.strip()
#                         # elif td[0].text == 'Product Lifecycle (PLM)':
#                         #     i['Lifecycle'] = td[1].text.strip()
#                         # elif td[0].text == 'Notes':
#                         #     i['Notes'] = td[1].text.strip()
#
#                     if save:
#                         id = save_product(i)
#                 except Exception as e:
#                     i['status'] = False
#
#             except Exception as e1:
#                 print("$$$$" + str(e1))
#                 i['Notes'] = ' was not found.Pleas check your Article Number.'
#                 i['status'] = False
#     except Exception as error:
#         print(error)
#
#
# def save_product(product):
#     try:
#         if product['replace']:
#             replace_code = product['replace'][0]['code']
#             save_product(product['replace'][0])
#         else:
#             replace_code = product['replace']
#         id = SysProducts(order_number=product['standard_ptn'], part_name='-', description=product['description'],
#                            image=product['image_url'], datasheet=product['datasheet'], rawcode=product['rawcode'],
#                          status='publish', brand='Siemens', pmd=0, replace_code=replace_code).insert()
#         return id
#     except Exception as e:
#         print(e)
#         return False
#
#
# def process_data(strdata, default):
#     request_number = tracking_code()
#     if default['remarks']:
#         pass
#     else:
#         default['remarks'] = 'مدت اعتبار قیمت و موجودی اقلام یک روز می باشد.\nنحوه ی تسویه: به صورت نقد'
#     codels = strdata.strip().split("\n")
#
#     codels = list(OrderedDict.fromkeys(codels))
#
#     vip_js = vip_get_data(codels, default)
#     return vip_js
#
#
# def get_euro():
#     try:
#         agent = {
#             "User-Agent": 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
#         }
#         url = 'https://www.tgju.org/'
#         req = requests.get(url, headers=agent)
#         req = req.text
#         bs = BeautifulSoup(req, "html.parser")
#         euro = bs.find("li", {"id": "l-price_eur"})
#         euro = euro.find("span", {"class": "info-price"}).text.strip()
#     except Exception as e:
#         print(e)
#         return False
#     return euro
