# !/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
import datetime
import json
import os
import random
import re
from collections import OrderedDict

import requests
import xlsxwriter
from bs4 import BeautifulSoup
from khayyam import JalaliDatetime

from configs import Configs
from web_client.models.bot_db_models import SysLogs, SysProducts, SysPmdRatio, mysql_db, SysSource


def tracking_code():
    try:
        dt = JalaliDatetime(datetime.datetime.now())
        d = dt.strftime("%Y%m%d")[2:]
        t = random.randint(101, 999)
        request_number = '#N{}_{}'.format(d, t)
        while SysLogs(request_number=request_number).get_all_by_request_number():
            t = random.randint(101, 999)
            request_number = '#N{}_{}'.format(d, t)
    except Exception as e:
        request_number = False

    if not mysql_db.is_closed():
        mysql_db.close()
    return request_number


def vip_get_data(ls, parameter):
    js = []
    try:
        for i in ls:
            res = False
            item = dict(
                id=0,
                code='',
                rawcode='',
                description=False,
                standard_ptn=False,
                order_number=False,
                weight=False,
                coo=False,
                Lifecycle=False,
                Notes='',
                datasheet=False,
                image_url=False,
                replace=False,
                replace_code='',
                status=False,
                pmd=1,
                dt_pmd_ratio=0,
                dt_ratio_root='',
                price_group_ww=0,
                price_group_us=0,
                product_dimensions=0,
                packaging_dimensions=0,
                last_modified=JalaliDatetime(datetime.datetime.now()).strftime("%Y/%m/%d - %H:%M"),

                qty=1,
                price=0,
                currency='IRR',
                mydiscount=float(parameter['my_ratio']) if parameter['my_ratio'] else 0,
                customertdiscount=float(parameter['customer_ratio']) if parameter['customer_ratio'] else 0,
                Purchase_price=0,
                Sales_price=0,
                profit=0,
                Statement=parameter['statement'],
                deliver=parameter['delivery'],
                source=parameter['source'],
                msg={"msg": "", "from": ""},
                user="",
                sourcels=[],
                euro=parameter['euro'] if parameter['euro'] else 0,
                logs=[],
                old_cod='',
                change_flag=False,
            )

            try:
                # ===========source=====================1=
                tmp = my_rsplit(i, '&')
                if tmp:
                    if tmp[1].strip() != '':
                        item['source'] = tmp[1].strip()
                    i = tmp[0].strip()
                # ===========delivery time===============2=
                tmp = my_rsplit(i, '~')
                if tmp:
                    if tmp[1].strip() != '':
                        item['deliver'] = tmp[1].strip()
                    i = tmp[0].strip()
                # ===========statement===================3=
                tmp = my_rsplit(i, '!')
                if tmp:
                    if tmp[1].strip() != '':
                        item['Statement'] = tmp[1].strip()
                    i = tmp[0].strip()
                # ===========price=======================4=
                tmp = my_rsplit(i, '=')
                if tmp:
                    try:
                        if not ''.join(filter(str.isdigit, tmp[1].strip()[0])):
                            code = tmp[0].split("/", 1)[0].strip()
                            rawcode = code.replace(' ', '').replace('_', '').replace('o', '0').replace('O',
                                                                                                       '0').replace('-',
                                                                                                                    '')
                            pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
                            if pr:
                                # item.update(pr)
                                log_price = get_price(tmp[1], pr)
                                if log_price['log']:
                                    item['price'] = int(log_price['log']['price'])
                                    item['date'] = str(log_price['log']['request_date'])
                                    item['user'] = str(log_price['log']['u_id'])
                                    item['source'] = str(log_price['log']['source'])
                                    item['msg']['from'] = "db"
                                    item['euro'] = log_price['log']['euro']
                                    item['doller'] = log_price['log']['doller']
                                    _prc=log_price['str']
                                    if _prc:
                                        strr = tmp[1].split(_prc, 1)[1]
                                        tmp2 = my_rsplit(strr, '*')
                                        if tmp2:
                                            _customertdiscount = re.findall('[-]?[0-9,.]+', tmp2[1])
                                            if _customertdiscount:
                                                item['customertdiscount'] = _customertdiscount[0]
                                                tmp3 = my_rsplit(tmp2[0], '*')
                                                if tmp3:
                                                    _mydiscount = re.findall('[-]?[0-9,.]+', tmp3[1])
                                                    if _mydiscount:
                                                        item['mydiscount'] = _mydiscount[0]
                                else:
                                    item['msg']['msg'] = "Product Recent Price Not Found!"
                            else:
                                item['msg']['msg'] = "Product Not Found!"

                        else:
                            _prc = re.findall('[0-9,.]+', tmp[1])
                            # print(_prc)
                            if _prc:
                                item['price'] = int(re.sub(r'[^\d.]', '', _prc[0]))
                                # =============currency=and=discount=====================
                                _currency = tmp[1].split(_prc[0], 1)[1]
                                if _currency.strip() != ''and item['price']:
                                    tmp2 = my_rsplit(_currency, '*')
                                    if tmp2:
                                        _customertdiscount = re.findall('[-]?[0-9,.]+', tmp2[1])
                                        if _customertdiscount:
                                            item['customertdiscount'] = _customertdiscount[0]
                                            tmp3 = my_rsplit(tmp2[0], '*')
                                            if tmp3:
                                                _mydiscount = re.findall('[-]?[0-9,.]+', tmp3[1])
                                                if _mydiscount:
                                                    item['mydiscount'] = _mydiscount[0]
                                                    if tmp3[0].strip() != '':
                                                        item['currency'] = tmp3[0].strip()
                                                    else:
                                                        item['currency'] = 'IRR'
                                                else:
                                                    item['currency'] = tmp2[0]
                                            else:
                                                item['currency'] = tmp2[0]
                                        else:
                                            item['currency'] = _currency
                                    else:
                                        item['currency'] = _currency
                                else:
                                    item['currency'] = 'IRR'
                        i = tmp[0].strip()
                    except Exception as er:
                        price = tmp[1].strip()
                        print(er, price)
                # ==========code=qty======================
                tmp = my_rsplit(i, '/')
                if tmp:
                    if tmp[1].strip() != '':
                        _qty = re.findall('[0-9,.]+', tmp[1])
                        if _qty:
                            item['qty'] = _qty[0]
                    item['code'] = tmp[0].strip()
                else:
                    item['code'] = i.strip()
            except Exception as er:
                pass

            rawcode = item['code'].replace(' ', '').replace('_', '').replace('o', '0').replace('O', '0').replace('-','')
            pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
            if pr:
                item['status'] = True
                item.update(pr)
                item['weight'] = [item['weight'] , "Kg"]
                item['sourcels'] = get_all_source(pr)
                dt = dt_pmd_ratio_search(item['code'].replace(" ",""))
                if dt:
                    item['dt_pmd_ratio'] = dt['value']
                    item['dt_ratio_root'] = dt['root_name']

            else:
                pass
                # siemens_request([item], True)

            if item['price']:
                if float(item['mydiscount']):
                    md = 1 + float(float(item['mydiscount']) / 100)
                    item['Purchase_price'] = int((md * int(item['price'])) / 10000) * 10000

                else:
                    item['Purchase_price'] = item['price']
                if float(item['customertdiscount']):
                    cd = 1 + float(float(item['customertdiscount']) / 100)
                    salesP = (item['Purchase_price']) * cd

                    if salesP > 10000:
                        rond = salesP % 10000
                        salesP = salesP // 10000
                        if rond < 5000:
                            item['Sales_price'] = int(salesP * 10000)
                        else:
                            item['Sales_price'] = int((salesP + 1) * 10000)
                    elif salesP > 1000:
                        rond = salesP % 1000
                        salesP = salesP // 1000
                        if rond < 500:
                            item['Sales_price'] = int(salesP * 1000)
                        else:
                            item['Sales_price'] = int((salesP + 1) * 1000)
                    else:
                        item['Sales_price'] = round(salesP)
                else:
                    item['Sales_price'] = item['Purchase_price']
                item['profit'] = (item['Sales_price'] - item['Purchase_price']) * int(item['qty'])
            js.append(item)
    except Exception as e:
        print('###' + str(e))

    if not mysql_db.is_closed():
        mysql_db.close()
    return js


def siemens_request(js, save):
    agent = {
        "User-Agent": 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    }
    try:
        for i in js:
            try:
                req = ''
                replace = False
                url = 'https://mall.industry.siemens.com/mall/en/WW/Catalog/Product/' + i['code']
                url = url.strip()
                i['url'] = url
                req = requests.get(url, headers=agent)
                req = req.text
                bs = BeautifulSoup(req, "html.parser")

                try:
                    # ----------------get description ------
                    description = bs.find("table", {"class": "ProductDetailsTable"})
                    trs = description.find_all("tr")
                    description = trs[2]
                    description = description.find_all("td")
                    i['description'] = description[1].text.strip()
                    i['status'] = True
                    i['rawcode'] = i['code'].replace(' ', '').replace('_', '').replace('o', '0').replace('O',
                                                                                                         '0').replace(
                        '-', '')
                    # ----------------get order number
                    prt_number = bs.find("span", {"class": 'productIdentifier'}).text.strip()
                    i['standard_ptn'] = prt_number

                    try:
                        i['image_url'] = bs.find("img", {"class": "productPicture"})['src']
                    except Exception as e:
                        i['image_url'] = 'https://pnap.ir/wp-content/uploads/public/no_image_pnap600.jpg'

                    try:
                        datasheet = bs.find("a", {"class": "pdfLink"})['href']
                        i['datasheet'] = 'https://mall.industry.siemens.com{}'.format(datasheet)
                    except Exception as e:
                        print(e, "datasheet except")
                        i['datasheet'] = url

                    try:
                        replace = bs.find("div", {"class": "bluelinesBox"})
                        if replace:
                            replace_link = replace.find("a", {"class": "internalLink"})
                            try:
                                replace = replace_link.text
                                i['replace'] = [dict(
                                    id=0,
                                    code=replace,
                                    rawcode='',
                                    pmd=1,
                                    description=False,
                                    standard_ptn=False,
                                    order_number=False,
                                    weight=0,
                                    coo=False,
                                    Lifecycle=False,
                                    Notes='',
                                    datasheet=False,
                                    image_url='https://pnap.ir/wp-content/uploads/public/no_image_pnap600.jpg',
                                    replace=False,
                                    replace_code='',
                                    status=True,
                                    price_group_ww=0,
                                    price_group_us=0,
                                    product_dimensions=0,
                                    packaging_dimensions=0,
                                    last_modified=JalaliDatetime(datetime.datetime.now()).strftime("%Y/%m/%d - %H:%M"),

                                    qty=1,
                                    price=0,
                                    currency='IRR',
                                    mydiscount=1,
                                    customertdiscount=1,
                                    Purchase_price=0,
                                    Sales_price=0,
                                    profit=0,
                                    Statement=False,
                                    deliver=False,
                                    source=False,
                                    msg={"msg": "", "from": ""},
                                    user="",
                                    sourcels=[],
                                    euro=0,
                                    logs=[],
                                    change_flag=False,
                                    old_cod = '',
                                    dt_pmd_ratio=0,
                                    dt_ratio_root=''
                                )]

                                siemens_request(i['replace'], False)

                            except Exception as e_replace:
                                i['replace'] = replace.text

                    except Exception as e:
                        pass
                    for w in trs:
                        td = w.find_all('td')
                        if td[0].text == 'Net Weight (kg)':
                            i['weight'] = td[1].text.strip()
                            i['weight'] = i['weight'].split(' ')
                            i['weight'][0] = i['weight'][0].replace(',', '.')
                        elif td[0].text == 'Country of origin':
                            i['coo'] = td[1].text.strip()
                        elif td[0].text == 'Product Lifecycle (PLM)':
                            i['Lifecycle'] = td[1].text.strip()
                        elif td[0].text == 'Notes':
                            i['Notes'] = td[1].text.strip()
                        # elif td[0].text == 'Price Group':
                        #     i['price_group_ww'] = td[1].text.strip()
                        # elif td[0].text == 'Product Dimensions (W x L x H)':
                        #     i['product_dimensions'] = td[1].text.strip()
                        # elif td[0].text == 'Packaging Dimension':
                        #     i['packaging_dimensions'] = td[1].text.strip()

                    # -----------------------------------------us request --------------------------------------------
                    # try:
                    #     url2 = 'https://mall.industry.siemens.com/mall/en/us/Catalog/Product/' + i['code']
                    #     url2 = url2.strip()
                    #     req2 = requests.get(url2, headers=agent)
                    #     req2 = req2.text
                    #     bs2 = BeautifulSoup(req2, "html.parser")
                    #     table = bs2.find("table", {"class": "ProductDetailsTable"})
                    #     trs2 = table.find_all("tr")
                    #     for w2 in trs2:
                    #         td2 = w2.find_all('td')
                    #         if td2[0].text == 'Price Group':
                    #             i['price_group_us'] = td2[1].text.strip()
                    # except Exception as e:
                    #     print(e)
                    # ------------------------------------------------------------------------------------------------

                    if save and i['status']:
                        id = save_product(i)
                        if id:
                            rawcode = str(id).replace(' ', '').replace('_', '').replace('o', '0').replace('O','0').replace('-', '')
                            prrrr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
                            i.update(prrrr)
                            # i['id']= prrrr['id']
                        print(i)
                        # if id:
                        #     ppr = SysProducts(rawcode=id).get_one_by_rawcode()
                        #     i['replace'] = ppr['order_number']
                except Exception as e:
                    i['status'] = False

            except Exception as e1:
                print("$$$$" + str(e1))
                i['Notes'] = ' was not found.Pleas check your Article Number.'
                i['status'] = False
    except Exception as error:
        print(error)

    if not mysql_db.is_closed():
        mysql_db.close()


def save_product(product):
    try:
        if product['replace']:
            if type(product['replace']) is list:
                replace_code = save_product(product['replace'][0])
            else:
                replace_code = product['replace']
        else:
            replace_code = False

        pr = SysProducts(rawcode=product['rawcode']).get_one_by_rawcode()
        if pr:
            if replace_code:
                id = SysProducts(id=pr['id']).update(replace_code=replace_code, lifecycle=product['Lifecycle'],
                                 notes=product['Notes'])
            else:
                id = SysProducts(id=pr['id']).update(lifecycle=product['Lifecycle'], notes=product['Notes'])
                # id = pr['order_number']
        else:
            if replace_code:
                id = SysProducts(order_number=product['standard_ptn'], part_name='-',
                                 description=product['description'],
                                 image=product['image_url'], datasheet=product['datasheet'], rawcode=product['rawcode'],
                                 status='publish', brand='Siemens', brand_group='', pmd=0,
                                 replace_code=replace_code,coo=product['coo'], lifecycle=product['Lifecycle'],
                                 weight=float(product['weight'][0])if product['weight']else 0,notes=product['Notes']).insert()
                if id:
                    product['id']=id
            else:
                id = SysProducts(order_number=product['standard_ptn'], part_name='-',
                                 description=product['description'],
                                 image=product['image_url'], datasheet=product['datasheet'], rawcode=product['rawcode'],
                                 status='publish', brand='Siemens', brand_group='', pmd=0,coo=product['coo'],
                                 lifecycle=product['Lifecycle'],weight=float(product['weight'][0])if product['weight']else 0,notes=product['Notes']).insert()
                product['id'] = id
        if not mysql_db.is_closed():
            mysql_db.close()
        if id:
            return product['standard_ptn']
        else:
            return False
    except Exception as e:
        if not mysql_db.is_closed():
            mysql_db.close()
        print(e)
        return False


def save_log(js, req_num,customer):
    try:
        msg_ls=dict()
        xxx=0
        for i in js:
            if i['status'] != 'removed':
                xxx+=1
                # print(xxx)
                if i['status'] and i['price']:
                    source = SysSource(name=i['source']).get_one_by_name()
                    if(source):
                        id = SysLogs(u_id=198804678, p_id=i['id'], price=i['price'], request_number=req_num,
                                     currency=i['currency'], source_id=source['id'], euro=i['euro'], doller=1,customer=customer).insert()
                        if id:
                            msg_ls[i['code']] = 'successfully'
                        else:
                            msg_ls[i['code']] = 'Error in save record'
                    else:
                        sid = SysSource(name=i['source'],description='new from list', status='publish').insert()
                        if sid:
                            id = SysLogs(u_id=198804678, p_id=i['id'], price=i['price'], request_number=req_num,
                                         currency=i['currency'], source_id=sid, euro=i['euro'], doller=1,customer=customer).insert()
                            if id:
                                msg_ls[i['code']] = 'successfully'
                            else:
                                msg_ls[i['code']] = 'Error in save record'
                        else:
                            msg_ls[i['code']] = 'Error in insert Source'
                else:
                    msg_ls[i['code']] = 'Invalid in code or price'
                    print('Invalid in code or price')
        if not mysql_db.is_closed():
            mysql_db.close()
        return  msg_ls
    except Exception as e:
        print(e)
    if not mysql_db.is_closed():
        mysql_db.close()
    return False


def process_data(strdata, default):
    if default['remarks']:
        pass
    else:
        default['remarks'] = 'مدت اعتبار قیمت و موجودی اقلام یک روز می باشد.\nنحوه ی تسویه: به صورت نقد'
    codels = strdata.strip().split("\n")

    # codels = list(OrderedDict.fromkeys(codels))

    vip_js = vip_get_data(codels, default)
    if not mysql_db.is_closed():
        mysql_db.close()
    return vip_js


def get_euro():
    try:
        agent = {
            "User-Agent": 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
        }
        url = 'https://www.tgju.org/'
        req = requests.get(url, headers=agent)
        req = req.text
        bs = BeautifulSoup(req, "html.parser")
        euro = bs.find("li", {"id": "l-price_eur"})
        euro = euro.find("span", {"class": "info-price"}).text.strip().replace(",", "")
        if not mysql_db.is_closed():
            mysql_db.close()
    except Exception as e:
        print(e)
        return False
    return euro


def dt_pmd_ratio_search(query):
    tmp = query
    res = SysPmdRatio(root_name=tmp).get_one_by_code()
    while not res and len(tmp) > 3:
        tmp = tmp[:-1]
        res = SysPmdRatio(root_name=tmp).get_one_by_code()
    if res:
        if not mysql_db.is_closed():
            mysql_db.close()
        return res
    else:
        if not mysql_db.is_closed():
            mysql_db.close()
        return {}


def create_customer_excel(js, request_number, remarks,customer,export_pmd_dt):
    try:
        _date = datetime.datetime.now()
        # print(Configs.app_root)
        ecxlname = "{} {}-{}.xlsx".format(request_number.replace("#", ""),customer,
                                                str(_date).replace(" ", "-").replace(":", '-'))
        path = os.path.join(Configs.app_root, "web_client", "statics", "temp", ecxlname)
        # print(path)
        workbook = xlsxwriter.Workbook(path, {'strings_to_numbers': True,
                                              'strings_to_formulas': True,
                                              'strings_to_urls': False})
        worksheet = workbook.add_worksheet()
        statment = workbook.add_format({'bg_color': 'yellow', 'text_wrap': True})
        statment.set_border()
        text_wrap = workbook.add_format({'text_wrap': True})
        text_wrap.set_border()
        color_red = workbook.add_format({'font_color': 'red'})
        color_red.set_border(1)
        color_red.set_align('vcenter')
        head = workbook.add_format({'bold': True, 'font_color': 'white', 'bg_color': 'black', 'align': 'center'})
        head.set_align('vcenter')
        _number = workbook.add_format({'num_format': '#,###'})
        _number.set_border()
        _number.set_align('vcenter')
        border = workbook.add_format()
        border.set_border()
        border.set_align('vcenter')

        bg_orange = workbook.add_format({'bg_color': 'orange', 'bold': True, 'font_color': 'white'})
        bg_orange.set_border()
        bg_orange.set_align('vcenter')
        bg_red = workbook.add_format({'bg_color': 'red', 'bold': True, 'font_color': 'white'})
        bg_red.set_border()
        bg_red.set_align('vcenter')
        bg_blue = workbook.add_format({'bg_color': '#ddedfb', 'bold': True})
        bg_blue.set_border()
        bg_blue.set_align('vcenter')
        bg_blue.set_align('left')

        worksheet.set_row(0, 20)
        worksheet.set_column('A:A', 3)
        worksheet.set_column('B:B', 23)
        worksheet.set_column('C:C', 40)
        worksheet.set_column('D:D', 7)
        worksheet.set_column('E:E', 12)
        worksheet.set_column('F:F', 10)
        worksheet.set_column('G:G', 3)
        worksheet.set_column('H:I', 13)
        worksheet.set_column('J:J', 13)
        worksheet.set_column('K:K', 9)
        worksheet.set_column('L:L', 25)
        worksheet.set_column('M:M', 7)
        worksheet.set_column('N:O', 19)
        worksheet.set_column('P:Q', 15)

        worksheet.write('A1', 'NO.', head)
        worksheet.write('B1', 'Siemens Article Number', head)
        worksheet.write('C1', 'Product Description', head)
        worksheet.write('D1', 'Weight', head)
        worksheet.write('E1', 'Weight Unit', head)
        worksheet.write('F1', 'COO', head)
        worksheet.write('G1', 'QTY', head)
        worksheet.write('H1', 'Unit Price', head)
        worksheet.write('I1', 'Total Price', head)
        worksheet.write('J1', 'Currency', head)
        worksheet.write('K1', 'Delivery Time', head)
        worksheet.write('L1', 'Initial Code (Replaced For)', head)
        worksheet.write('M1', 'Notes', head)
        worksheet.write('N1', 'DataSheet', head)
        worksheet.write('O1', 'Lifecycle', head)
        worksheet.write('P1', 'Total Weight', head)
        worksheet.write('Q1', 'Statement', head)
        if export_pmd_dt == 'true':
            worksheet.set_column('R:V', 15)
            worksheet.write('R1', 'PMD', head)
            worksheet.write('S1', 'Dt Ratio', head)
            worksheet.write('T1', 'Dt String', head)
            # worksheet.write('U1', 'Price Group WW', head)
            # worksheet.write('V1', 'Price Group US', head)
            # worksheet.write('W1', 'Product Dimensions', head)
            # worksheet.write('X1', 'Packaging Dimensions', head)


        row = 0
        for i in js:
            try:
                if i['status']!='removed':
                    row += 1
                    item = i
                    worksheet.write(row, 0, str(row), border)
                    if item['status']:
                        if item['replace']:
                            # if item['price'] or item['Statement']:
                            worksheet.write(row, 1, item['standard_ptn'] if item['standard_ptn'] else item['order_number'],
                                            border)
                            if type(item['replace']) == list:
                                worksheet.write(row, 11, item['replace'][0]['standard_ptn'], bg_orange)
                            else:
                                worksheet.write(row, 11, item['replace'], bg_orange)
                            # else:
                            #     if type(item['replace']) == list:
                            #         item = i['replace'][0]
                            #         worksheet.write(row, 11, i['order_number'], border)
                            #     worksheet.write(row, 1, item['standard_ptn'] if item['standard_ptn'] else item['order_number'],
                            #                     border)
                        else:
                            worksheet.write(row, 1, item['standard_ptn'] if item['standard_ptn'] else item['order_number'], border)
                            worksheet.write(row, 11, '', border)

                        if item['change_flag'] == 1:
                            worksheet.write(row, 11, item['old_cod'], bg_orange)
                        elif item['change_flag'] == 2:
                            worksheet.write(row, 11, item['old_cod'], color_red)

                        worksheet.write(row, 2, item['description'], text_wrap)
                        if item['weight']:
                            try:
                                if type(item['weight']) == list:
                                    worksheet.write_number(row, 3, float(item['weight'][0]), border)
                                    worksheet.write(row, 4, item['weight'][1], border)
                                else:
                                    worksheet.write_number(row, 3, float(item['weight']), border)
                                    worksheet.write(row, 4, 'kg', border)
                            except Exception as ewe:
                                worksheet.write_number(row, 3, float(0), border)
                                worksheet.write(row, 4, ewe, border)
                        else:
                            worksheet.write_number(row, 3, 0, border)
                            worksheet.write(row, 4, 'kg', border)
                        if item['coo']:
                            worksheet.write(row, 5, item['coo'], border)
                        worksheet.write_number(row, 6, int(item['qty']), border)
                        worksheet.write_number(row, 7, item['Sales_price'], _number)
                        worksheet.write_formula(row, 8, '=G{}*H{}'.format(row + 1, row + 1), _number)
                        worksheet.write(row, 9, item['currency'], border)
                        if item['deliver']:
                            worksheet.write(row, 10, item['deliver'], border)
                        else:
                            worksheet.write(row, 10, '', border)

                        if item['Notes']:
                            worksheet.write(row, 12, item['Notes'], color_red)
                        else:
                            worksheet.write(row, 12, '', border)
                        worksheet.write(row, 13, item['datasheet'], border)
                        worksheet.write(row, 14, item['Lifecycle'], border)
                        worksheet.write_formula(row, 15, '=G{}*D{}'.format(row + 1, row + 1), border)
                        if item['Statement']:
                            worksheet.write(row, 16, item['Statement'], statment)
                        else:
                            worksheet.write(row, 16, '', text_wrap)

                        if export_pmd_dt == 'true':
                            worksheet.write_number(row, 17, item['pmd'], bg_blue)
                            worksheet.write_number(row, 18, item['dt_pmd_ratio'], bg_blue)
                            worksheet.write(row, 19, item['dt_ratio_root'], bg_blue)
                            # worksheet.write(row, 20, item['price_group_ww'], bg_blue)
                            # worksheet.write(row, 21, item['price_group_us'], bg_blue)
                            # worksheet.write(row, 22, item['product_dimensions'], bg_blue)
                            # worksheet.write(row, 23, item['packaging_dimensions'], bg_blue)

                    else:
                        try:
                            worksheet.write(row, 1, item['code'], bg_red)
                            worksheet.write(row, 2, 'ERROR', border)
                            worksheet.write(row, 3, '', border)
                            # worksheet.write_url(row, 3, item['url'], string='siemens page')
                            worksheet.write(row, 4, '', border)
                            worksheet.write(row, 5, '', border)
                            worksheet.write_number(row, 6, int(item['qty']), border)
                            worksheet.write_number(row, 7, item['Sales_price'], _number)
                            worksheet.write_formula(row, 8, '=G{}*H{}'.format(row + 1, row + 1), _number)
                            worksheet.write(row, 9, '', border)
                            if item['deliver']:
                                worksheet.write(row, 10, item['deliver'], border)
                            else:
                                worksheet.write(row, 10, '', border)

                            worksheet.write(row, 11, '', border)
                            worksheet.write(row, 12, item['Notes'], color_red)
                            worksheet.write(row, 13, '', border)
                            worksheet.write(row, 14, '', border)
                            worksheet.write_number(row, 15, 0, border)
                            worksheet.write(row, 16, '', border)
                            if export_pmd_dt == 'true':
                                worksheet.write(row, 17, '', bg_blue)
                                worksheet.write(row, 18, '', bg_blue)
                                worksheet.write(row, 19, '', bg_blue)
                                # worksheet.write(row, 20, '', bg_blue)
                                # worksheet.write(row, 21, '', bg_blue)
                                # worksheet.write(row, 22, '', bg_blue)
                                # worksheet.write(row, 23, '', bg_blue)
                        except Exception as eo:
                            pass
            except Exception as errr:
                print('DsDsDs:' + str(errr))
        try:
            row += 1
            worksheet.write(row, 2, 'Total Net Weight', head)
            worksheet.write(row, 7, 'Total AMT', head)
            worksheet.write_formula(row, 3, '=SUM(P2:P{})'.format(row), border)  # total weight
            worksheet.write_formula(row, 6, '=SUM(G2:G{})'.format(row), border)  # sum qty
            worksheet.write_formula(row, 8, '=SUM(I2:I{})'.format(row), _number)  # sum price
            worksheet.write_formula(row, 15, '=SUM(P2:P{})'.format(row), border)

            if remarks:
                head_remark = workbook.add_format({'bold': True})
                head_remark.set_italic()
                head_remark.set_font_size(16)
                head_remark.set_underline()
                row += 2
                worksheet.write(row, 2, 'Remarks:', head_remark)
                remarks_ls = remarks.split('\n')
                for remark in remarks_ls:
                    if remark.strip() != '':
                        row += 1
                        worksheet.write(row, 2, remark, text_wrap)

            workbook.close()
            return ecxlname
        except Exception as e11:
            print('end', e11)
    except Exception as err:
        print(err)
    if not mysql_db.is_closed():
        mysql_db.close()


def telegram_sum_price_message(js, request_number):
    msg = '- Sum Purchase Price : {}<br><b>- TOT Sales Price : {}</b><br>- SumProfit : {}<br>- AvgDiscount : {}%<br>{}'
    sys = dict(sumPrice=0, avgDiscount=1, sumProfit=0, sumSalesPrice=0)
    for i in js:
        if i['price']:
            try:
                sys['sumSalesPrice'] += int(i['Sales_price']) * int(i['qty'])
                sys['sumPrice'] += int(i['Purchase_price']) * int(i['qty'])
                sys['sumProfit'] += i['profit']
            except Exception as e21:
                print('telegram_sum_price_message 1 : ' + str(e21))
    if sys['sumPrice']:
        try:
            if sys['sumProfit']:
                avgD = 100 / (sys['sumPrice'] / sys['sumProfit'])
                avgD = "%.2f" % avgD
            else:
                avgD = 0
            sys['sumPrice'] = "{:,}".format(sys['sumPrice'])
            sys['sumProfit'] = "{:,}".format(int(sys['sumProfit']))
            sys['sumSalesPrice'] = "{:,}".format(sys['sumSalesPrice'])

            msg = msg.format(sys['sumPrice'], sys['sumSalesPrice'], sys['sumProfit'], avgD, request_number)
            return msg
        except Exception as eeee:
            print("/\/\/\/" + str(eeee))
    if not mysql_db.is_closed():
        mysql_db.close()


def get_all_source(pr=None):
    try:
        if pr:
            sourc_ls = SysLogs(p_id=pr['id']).get_all_source_product()
            # print(sourc_ls)
        else:
            sourc_ls = SysSource().get_all()
        if not mysql_db.is_closed():
            mysql_db.close()
        return sourc_ls
    except Exception as e:
        print(e)
        if not mysql_db.is_closed():
            mysql_db.close()
        return []


def write_text(ls, name):
    try:
        path = os.path.join(Configs.app_root, "web_client", "statics", "temp", name + '.txt')
        with codecs.open(path, 'w', encoding='utf8') as f:
            for i in ls:
                f.write(i['order_number'] + '\n')
        f.close()
        return path
    except Exception as e:
        print(e)
        return False


def my_rsplit(str, chr):
    try:
        tmp = str.rsplit(chr, 1)
        if len(tmp) > 1:
            return tmp
        else:
            return False
    except Exception as e:
        print(e)
        return False


def get_price(str, pr):
    res = False
    try:
        str = str.split("*")[0].strip()
        print(str[0].lower())
        if str[:4].lower() == 'last':
            try:
                ex = re.findall(r"[(](\w+)[)]", str)[0]
                res = SysLogs(p_id=pr['id'], source_id=ex).get_last_by_source()
            except Exception:
                res = SysLogs(p_id=pr['id']).get_last()
        elif str[:3].lower() == 'max':
            try:
                ex = re.findall(r"[(](\w+)[)]", str)[0]
                delta = ''.join(filter(str.isdigit, ex))
                if delta:
                    res = SysLogs(p_id=pr['id']).get_max_price(int(delta))
                else:
                    res = SysLogs(p_id=pr['id'], source_id=ex).get_max_price_by_source()
            except Exception as ett:
                pass
            if not res:
                res = SysLogs(p_id=pr['id']).get_max_price()
        elif str[:3].lower() == 'mim':
            try:
                ex = re.findall(r"[(](\w+)[)]", str)[0]
                delta = ''.join(filter(str.isdigit, ex))
                if delta:
                    res = SysLogs(p_id=pr['id']).get_min_price(int(delta))
                else:
                    res = SysLogs(p_id=pr['id'], source_id=ex).get_min_price_by_source()
            except Exception:
                pass
            if not res:
                res = SysLogs(p_id=pr['id']).get_min_price()
        else:
            res = SysLogs(p_id=pr['id'], source_id=str).get_last_by_source()
    except Exception as e:
        print(e)

    if not mysql_db.is_closed():
        mysql_db.close()
    return dict(str=str, log=res)

def get_pmd(code_ls):
    js=[]
    try:
        for i in code_ls:
            if i.strip()!='':
                rawcode = i.replace(' ', '').replace('_', '').replace('o', '0').replace('O','0').replace('-','')
                pr = SysProducts(rawcode=rawcode).get_one_by_rawcode()
                if pr:
                    js.append(pr)
                else:
                    js.append({'order_number': i, 'pmd':0, 'last_modified':''})
    except Exception as e:
        print("get_pmd() :",e)
    if not mysql_db.is_closed():
        mysql_db.close()
    return js

def create_pmd_excel(js):
    try:
        _date = datetime.datetime.now()
        # print(Configs.app_root)
        ecxlname = "pmd_{}.xlsx".format(str(_date).replace(" ", "-").replace(":", '-'))
        path = os.path.join(Configs.app_root, "web_client", "statics", "temp", ecxlname)
        workbook = xlsxwriter.Workbook(path, {'strings_to_numbers': True,
                                              'strings_to_formulas': True,
                                              'strings_to_urls': False})
        worksheet = workbook.add_worksheet()
        head = workbook.add_format({'bold': True, 'font_color': 'white', 'bg_color': 'black', 'align': 'center'})
        head.set_align('vcenter')
        border = workbook.add_format()
        border.set_border()
        border.set_align('vcenter')

        bg_orange = workbook.add_format({'bg_color': 'orange', 'bold': True, 'font_color': 'white'})
        bg_orange.set_border()
        bg_orange.set_align('vcenter')
        bg_red = workbook.add_format({'bg_color': 'red', 'bold': True, 'font_color': 'white'})
        bg_red.set_border()
        bg_red.set_align('vcenter')

        worksheet.set_row(0, 20)
        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 25)
        worksheet.set_column('C:C', 10)
        worksheet.set_column('D:D', 30)

        worksheet.write('A1', 'NO.', head)
        worksheet.write('B1', 'Siemens Article Number', head)
        worksheet.write('C1', 'PMD', head)
        worksheet.write('D1', 'Last Modified', head)

        row = 0
        for i in js:
            try:
                row += 1
                item = i
                worksheet.write(row, 0, str(row), border)

                if item['pmd']:
                    worksheet.write(row, 1, i['order_number'], border)
                    worksheet.write(row, 2, str(i['pmd']), border)
                    worksheet.write(row, 3, str(i['last_modified']), border)
                else:
                    if item['last_modified']:
                        worksheet.write(row, 1, i['order_number'], bg_orange)
                    else:
                        worksheet.write(row, 1, i['order_number'], bg_red)
                    worksheet.write(row, 2, '-', border)
                    worksheet.write(row, 3, '-', border)
            except Exception as errr:
                print('pmd excel error: ' + str(errr))

        workbook.close()
        if not mysql_db.is_closed():
            mysql_db.close()
        return ecxlname
    except Exception as e:
        print(e)
        if not mysql_db.is_closed():
            mysql_db.close()
        return False
#
# root = ['6ES72', '6GK72', '6ES7901-3C', '6ES7901-3D', '6ES7253', '6ES7241', '6ES7195-0', '6ES7195-1', '6ES7195-7',
#         '6ES7307', '6ES731', '6ES7321', '6ES7322', '6ES7323', '6ES7331', '6ES7315-2EH', '6ES7332', '6ES7334', '6ES7338',
#         '6ES734', '6ES736', '6ES7390', '6ES7392', '6ES7953', '6ES7971', '6ES7972-0A', '6ES7972-0B', '6ES7972-0C',
#         '6ES7972-0D', '6GK1500-0D', '6GK1500-0E', '6GK1500-0F', '6GK734', '6ES712', '6ES713X-0', '6ES713X-1', '6ES714',
#         '6ES7193-0', '6ES7193-1', '6ES7193-8', '6ES713', '6ES7131-4', '6ES7132-4', '6ES7134-4', '6ES7135-4',
#         '6ES7138-4', '6ES7151', '6ES7193', '6ES7153', '6ES74', '6ES7952-1', '6ES7960-1AA00', '6ES7960-1AA04',
#         '6ES7901-0', '6ES7901-1', '6ES7901-2', '6ES7902', '6GK116', '6GK156', '6GK157', '6GK1901', '6GK1905', '6GK74',
#         '6GK155', '6BK', '6ES7647', '6ES7650', '6ES7652', '6AG41', '6AV77', '6AV78', '6GF', '6ES7157', '6ES7158',
#         '6ES7197', '6ES718', '6AV6640', '6AV6641', '6AV6642', '6AV6643', '6AV6644', '6AV6574-2AC', '6AV662', '6AV665',
#         '6GK110', '6GK12', '6GK14', '6GK1500-3', '6GK1502', '6GK1503', '6GK19', '6EP106', '6EP1331', '6EP1332',
#         '6EP1333', '6EP1334', '6EP1336', '6EP1337', '6EP14', '6EP19', '6EP8', '6ES7654', '6ES7656', '6ES7658',
#         '6ES7810', '6ES7822', '6ES7652', '6ES7658', '6AV637', '6AV638', '6AV661', '6XV1830-0EH10', '6XV1840-2AH10',
#         '6XV1', '6ES7833', '6GK171', '6GK5', '6ES7321-7', '6ES7322-5', '6ES7322-8', '6ES7326', '6ES7336', '6ES7331-7T',
#         '6ES7332-5T', '6ES7332-8T', '6ES7870', '6ES7315-6FF', '6ES7315-2FH', '6ES7317-6FF', '6ES7317-2FK',
#         '6ES7318-3FL', '6ES7350', '6ES7351', '6ES7352', '6ES7353', '6ES7354', '6ES7355', '6ES7357', '6ES7715',
#         '6ES779X', '6ES7951', 'C7', 'GWK', '2XV', '6AG1', '6DD', '6DR', '7MH', '6DL', '6DM', '6DS', '6GT', '6NH', '6SY',
#         '6SE6400-0A', '6SE6400-0B', '6SE6400-0E', '6SE6400-1P', '6SE6420-2UC', '6SE6440-2UC', '6SE6430-2U',
#         '6SE6440-2UD', '6SE6420-2UD', '6SE6420-2A', '6SE6430-2A', '6SE6440-2A', '6SE6400', '6SL3211-0A', '6SL3255-0A',
#         '6SL', '6SE6411', '6FC5500-0A', '1FT', '1FK', '1FL', '1PH', '1XP', '6FC', '6FX', '6SE7', '6SN', '6SX', '6RA',
#         '6RY', '6RX', '7ME', '7MF', '7ML', '7MA', '7MB', '7MC', '7MR', '3VL', '3RU11', '3RU19', '3RV', '3RN', '3RT',
#         '3RH', '3WL', '3KA', '3KE', '3KL', '3KM', '3KX', '3KY', '3LD', '3NA', '3NC', '3ND', '3NE', '3NG', '3NH', '3NJ',
#         '3NP', '3NW', '3NX', '3NY', '3RA', '3RB', '3RE', '3RF', '3RG', '3RK', '3RS', '3RW', '3RX', '3SB', '3SE', '3SF',
#         '3SR', '3SX', '3SY', '3TB', '3TC', '3TD', '3TE', '3TF', '3TG', '3TH', '3TK', '3TX', '3TY', '3UF', '3UG', '3UL',
#         '3VF', '3VU', '3WN', '3WX', '3ZS', '3ZX', '4AM', '4AP', '4AT', '4AU', '4AV', '4AX', '4BT', '4BU', '4CH', '4CJ',
#         '4CP', '4CQ', '4FD', '4FK', '4FL', '4NC', '4NX', '5SA', '5SB', '5SC', '5SD', '5SE', '5SF', '5SG', '5SH', '5SJ',
#         '5SM', '5SP', '5ST', '5SU', '5SW', '5SX', '5SY', '5SZ', '5TE', '5TG', '7KT', '7PV', '7PX', '8GD', '8GE', '8GF',
#         '8GK', '8HA', '8HC', '8HP', '8JH', '8JK', '8MC', '8ME', '8MF', '8MR', '8UB', '8UC', '8US', '8WA', '8WC', '8WD',
#         'LZX', '3D', '3CJ1', '3CJ2', '3DX', '3CX', 'PFS:', '3GD', '3GH', '3EE', '3EF', '3EX', '3AE', '3AH', '3AC',
#         '3AX', '3AY', 'SWB:', '3SV9', '3TL6', '3TL7', '3TL8', '3TX', '3TY', '3RT15', 'VS', '3FA', '3FH', '3FM', '4M',
#         '3CG', '3AF', '3AG', '3AD', '7PA2', '7PA3', '7RE28', '7RW600', '7SA522', '7SA6', '7SA53', '7SA510', '7SA511',
#         '7SA513', '7SA518', '7SA519', '7SD600', '7SD610', '7SD502', '7SD503', '7SD511', '7SD512', '7SJ45', '7SJ46',
#         '7SJ600', '7SJ601', '7SJ602', '7SJ61', '7SJ62', '7SJ63', '7SJ64', '7SJ80', '7SJ50', '7SJ511', '7SJ512',
#         '7SJ531', '7SJ551', '4AM5', '6XV81', '7KE6000-8G', '7XV5', '7XV6', '6MD61', '6MD63', '6MD662', '6MD664',
#         '6MD665', '7UW50', '7VE6', '7VH60', '7VK61', '7VP15', '7XV75', 'PHO:32', '7UT612', '7UT613', '7UT63', '7SK80',
#         '7SN600', '7SS52', '7SS60', '7ST61', '7ST63', '7SV600', '7TS15', '7TS16', '7UM61', '7UM62', '7SS50', '7UT51',
#         '7SV512', '7SV73', '7UM511', '7UM512', '7UM515', '7UM516', '7VE51', '7VH80', '7VH83', '7VK512', '6ES72',
#         '6GK72', '6ES7901-3C', '6ES7901-3D', '6ES7253', '6ES7241']
# val = ["0.68", "0.87", "0.51", "0.48", "0.87", "0.87", "0.79", "0.60", "0.54", "0.66", "0.65", "0.66", "0.67", "0.68",
#        "0.69", "0.70", "0.71", "0.72", "0.73", "0.74", "0.75", "0.76", "0.77", "0.78", "0.79", "0.80", "0.81", "0.82",
#        "0.83", "0.84", "0.85", "0.86", "0.87", "0.94", "0.89", "0.60", "0.94", "0.89", "0.79", "0.89", "0.85", "0.85",
#        "0.60", "0.85", "0.57", "0.60", "0.81", "0.85", "0.63", "0.63", "0.63", "1.05", "0.66", "0.66", "0.92", "0.92",
#        "0.92", "0.66", "0.60", "1.04", "0.57", "0.89", "0.88", "1.04", "1.09", "1.09", "1.09", "1.09", "1.13", "1.00",
#        "1.00", "1.00", "0.94", "1.04", "1.04", "1.05", "0.72", "0.72", "0.48", "0.51", "0.85", "0.87", "0.87", "0.87",
#        "0.72", "1.09", "1.09", "1.09", "1.09", "1.09", "1.09", "0.96", "0.94", "0.94", "0.72", "0.72", "0.96", "0.96",
#        "0.96", "0.78", "0.96", "0.85", "0.85", "0.85", "0.79", "0.79", "0.79", "0.79", "0.79", "0.79", "0.79", "1.31",
#        "1.31", "1.13", "0.87", "0.96", "1.02", "0.81", "0.81", "0.81", "0.98", "0.98", "0.83", "0.83", "0.83", "0.87",
#        "1.05", "1.16", "1.16", "1.16", "1.16", "0.81", "1.05", "1.16", "1.16", "1.16", "1.16", "1.16", "1.09", "1.21",
#        "0.96", "1.13", "1.13", "1.22", "1.09", "1.26", "1.00", "1.00", "1.13", "1.13", "1.13", "1.13", "1.13", "1.13",
#        "1.08", "0.87", "0.92", "0.92", "0.87", "0.87", "0.74", "0.74", "0.74", "0.87", "0.83", "0.83", "1.00", "0.83",
#        "0.83", "1.05", "1.18", "0.74", "1.37", "1.52", "1.52", "1.52", "1.13", "1.26", "1.26", "1.26", "1.26", "1.26",
#        "1.13", "1.26", "1.26", "1.05", "1.16", "1.16", "1.05", "1.16", "1.16", "1.16", "0.61", "0.61", "0.61", "0.61",
#        "0.61", "0.61", "0.61", "0.61", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.94", "0.87", "0.94", "0.94", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87", "0.87",
#        "0.87", "0.87", "0.87", "1.24", "1.38", "1.38", "1.38", "1.38", "1.38", "1.24", "1.38", "1.24", "1.38", "1.38",
#        "1.39", "1.54", "1.54", "1.54", "1.54", "1.54", "1.54", "1.39", "1.54", "1.54", "1.24", "1.38", "1.38", "1.24",
#        "1.39", "1.54", "1.54", "1.39", "1.39", "1.39", "1.54", "1.24", "1.24", "1.38", "1.24", "1.38", "1.24", "1.38",
#        "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.24", "1.38", "1.38", "1.38", "1.38", "1.38", "1.24", "1.38",
#        "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.24",
#        "1.38", "1.38", "1.38", "1.38", "1.24", "1.38", "1.38", "1.38", "1.38", "1.24", "1.38", "1.38", "1.38", "1.38",
#        "1.38", "1.38", "1.24", "1.38", "1.38", "1.24", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38",
#        "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38", "1.38",
#        "0.68", "0.87", "0.68", "0.68", "0.87", "0.87"]
# print(len(root))
# print(len(val))
#
# for i in range (0,len(root)):
#     id = SysPmdRatio(root_name=root[i], value=val[i]).insert()
#     if id:
#         print(id)
