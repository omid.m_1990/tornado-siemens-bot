from tornado import web, ioloop

from configs import Configs
from api.urls import urls_list

if __name__ == "__main__":
    app = web.Application(
        urls_list,
        debug=True,
        autoreload=False,

    )
    app.listen(Configs.api['port'])
    ioloop.IOLoop.current().start()
