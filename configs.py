#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from peewee import *

class Configs:
    project_path = os.path.dirname(__file__)
    app_root = os.path.join(os.path.dirname(__file__),"")

    web = dict(
        port=8888,
        static_path=os.path.join(project_path, "web_client", "statics"),
        template_path=os.path.join(project_path, "web_client", "templates")
    )

    api = dict(
        port=8889
    )

    mysql_db = MySQLDatabase('pnap_bot_db', user='root', password='', host='localhost', port=3306, charset="utf8")