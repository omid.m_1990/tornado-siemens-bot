#!/usr/bin/env python
# -*- coding: utf-8 -*-
import tornado
from tornado import web, ioloop

from tornado.options import options, define

from configs import Configs
from web_client.urls import urls_list

define("port", default=Configs.web['port'], help="run on the given port", type=int)

if __name__ == "__main__":
    tornado.options.parse_command_line()

    app = web.Application(
        urls_list,
        debug=True,
        autoreload=False,
        static_path=Configs.web['static_path'],
        template_path=Configs.web['template_path']
    )
    app.listen(options.port)
    ioloop.IOLoop.current().start()
